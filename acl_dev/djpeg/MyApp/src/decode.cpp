#include "decode.h"
#include "utils.h"
#include <stdint.h>
#include <string>

Decode::Decode() {}

Decode::~Decode() {
    DestroyDecodeResource();
}

void* Decode::GetDeviceBufferOfPicture(const PicDesc &picDesc, uint32_t &devPicBufferSize) {
    if (picDesc.picName.empty()) {
        ERROR_LOG("picture file name is empty");
        return nullptr;
    }

    FILE *fp = fopen(picDesc.picName.c_str(), "rb");
    if (fp == nullptr) {
        ERROR_LOG("open file %s failed", picDesc.picName.c_str());
        return nullptr;
    }

    fseek(fp, 0, SEEK_END);
    uint32_t fileLen = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    uint32_t inputBuffSize = fileLen;

    char* inputBuff = new(std::nothrow) char[inputBuffSize];
    size_t readSize = fread(inputBuff, sizeof(char), inputBuffSize, fp);
    if (readSize < inputBuffSize) {
        ERROR_LOG("need read file %s %u bytes, but only %zu readed", picDesc.picName.c_str(), inputBuffSize, readSize);
        delete[] inputBuff;
        fclose(fp);
        return nullptr;
    }

    acldvppJpegFormat format;
    aclError aclRet = acldvppJpegGetImageInfoV2(inputBuff, inputBuffSize, const_cast<uint32_t*>(&picDesc.width),
                                                const_cast<uint32_t*>(&picDesc.height), nullptr, &format);
    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("get jpeg image info failed, errorCode is %d", static_cast<int32_t>(aclRet));
        delete[] inputBuff;
        fclose(fp);
        return nullptr;
    }

    INFO_LOG("get jpeg image info successed, width=%d, height=%d, format=%d, jpegDecodeSize=%d", picDesc.width, picDesc.height, format, picDesc.jpegDecodeSize);

    // when you run, from the output, we can see that the original format is ACL_JPEG_CSS_420,
    // so it can be decoded as PIXEL_FORMAT_YUV_SEMIPLANAR_420 or PIXEL_FORMAT_YVU_SEMIPLANAR_420
    aclRet = acldvppJpegPredictDecSize(inputBuff, inputBuffSize, PIXEL_FORMAT_YUV_SEMIPLANAR_420, const_cast<uint32_t*>(&picDesc.jpegDecodeSize));
    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("get jpeg decode size failed, errorCode is %d", static_cast<int32_t>(aclRet));
        delete[] inputBuff;
        fclose(fp);
        return nullptr;
    }

    void *inBufferDev = nullptr;
    aclError ret = acldvppMalloc(&inBufferDev, inputBuffSize);
    if (ret !=  ACL_SUCCESS) {
        delete[] inputBuff;
        ERROR_LOG("malloc device data buffer failed, aclRet is %d", ret);
        fclose(fp);
        return nullptr;
    }

    if (Utils::GetRunMode()) {
        ret = aclrtMemcpy(inBufferDev, inputBuffSize, inputBuff, inputBuffSize, ACL_MEMCPY_HOST_TO_DEVICE);
    }
    else {
        ret = aclrtMemcpy(inBufferDev, inputBuffSize, inputBuff, inputBuffSize, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("memcpy failed. Input host buffer size is %u",
        inputBuffSize);
        acldvppFree(inBufferDev);
        delete[] inputBuff;
        fclose(fp);
        return nullptr;
    }

    delete[] inputBuff;
    devPicBufferSize = inputBuffSize;
    fclose(fp);
    return inBufferDev;
}

Result Decode::SaveDvppOutputData(const char *fileName, const void *devPtr, uint32_t dataSize) {
    FILE *outFileFp = fopen(fileName, "wb+");
    if (nullptr == outFileFp) {
        ERROR_LOG("fopen out file %s failed.", fileName);
        return FAILED;
    }
    if (Utils::GetRunMode()) {
        void* hostPtr = nullptr;
        aclError aclRet = aclrtMallocHost(&hostPtr, dataSize);
        if (aclRet != ACL_SUCCESS) {
            ERROR_LOG("malloc host data buffer failed, aclRet is %d", aclRet);
            fclose(outFileFp);
            return FAILED;
        }

        aclRet = aclrtMemcpy(hostPtr, dataSize, devPtr, dataSize, ACL_MEMCPY_DEVICE_TO_HOST);
        if (aclRet != ACL_SUCCESS) {
            ERROR_LOG("dvpp output memcpy to host failed, aclRet is %d", aclRet);
            (void)aclrtFreeHost(hostPtr);
            fclose(outFileFp);
            return FAILED;
        }
        size_t writeSize = fwrite(hostPtr, sizeof(char), dataSize, outFileFp);
        if (writeSize != dataSize) {
            ERROR_LOG("need write %u bytes to %s, but only write %zu bytes.",
            dataSize, fileName, writeSize);
            (void)aclrtFreeHost(hostPtr);
            fclose(outFileFp);
            return FAILED;
        }
        (void)aclrtFreeHost(hostPtr);
    }
    else {
        size_t writeSize = fwrite(devPtr, sizeof(char), dataSize, outFileFp);
        if (writeSize != dataSize) {
            ERROR_LOG("need write %u bytes to %s, but only write %zu bytes.",
            dataSize, fileName, writeSize);
            fclose(outFileFp);
            return FAILED;
        }
    }
    fflush(outFileFp);
    fclose(outFileFp);
    return SUCCESS;
}

void Decode::SetInput(void *inDevBuffer, int inDevBufferSize, int inputWidth, int inputHeight) {
    inDevBuffer_ = inDevBuffer;
    inDevBufferSize_ = inDevBufferSize;
    inputWidth_ = inputWidth;
    inputHeight_ = inputHeight;
}

void Decode::DestroyDecodeResource() {
    inDevBuffer_ = nullptr;
    if (decodeOutputDesc_ != nullptr) {
        acldvppDestroyPicDesc(decodeOutputDesc_);
        decodeOutputDesc_ = nullptr;
    }
}

Result Decode::StartDecode(PicDesc &testPic, std::string image_path, aclrtStream &stream_) {
    // 1. 读取图片至内存
    uint32_t devPicBufferSize;
    void *picDevBuffer = GetDeviceBufferOfPicture(testPic, devPicBufferSize);
    if (picDevBuffer == nullptr) {
        ERROR_LOG("get pic device buffer failed,index is 0");
        return FAILED;
    }

    // 2. 创建输入图片通道和描述
    dvppChannelDesc_ = acldvppCreateChannelDesc();
    acldvppCreateChannel(dvppChannelDesc_);
    INFO_LOG("dvpp init resource success");

    // 3. 设置输入图片信息
    SetInput(picDevBuffer, devPicBufferSize, testPic.width, testPic.height);

    // 4. 初始化输出图片描述信息
    uint32_t decodeOutWidthStride = (inputWidth_ + 127) / 128 * 128; // 128-byte alignment
    uint32_t decodeOutHeightStride = (inputHeight_ + 15) / 16 * 16; // 16-byte alignment

    // use acldvppJpegPredictDecSize to get output size.
    // uint32_t decodeOutBufferSize = decodeOutWidthStride * decodeOutHeightStride * 3 / 2; // yuv format size
    // uint32_t decodeOutBufferSize = testPic.jpegDecodeSize;
    aclError ret = acldvppMalloc(&decodeOutDevBuffer_, testPic.jpegDecodeSize);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acldvppMalloc jpegOutBufferDev failed, ret = %d", ret);
        return FAILED;
    }

    decodeOutputDesc_ = acldvppCreatePicDesc();
    if (decodeOutputDesc_ == nullptr) {
        ERROR_LOG("acldvppCreatePicDesc decodeOutputDesc failed");
        return FAILED;
    }

    // 5. 设置输入图片描述信息
    acldvppSetPicDescData(decodeOutputDesc_, decodeOutDevBuffer_);
    // here the format should be same with the value you set when you get decodeOutBufferSize from
    acldvppSetPicDescFormat(decodeOutputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(decodeOutputDesc_, inputWidth_);
    acldvppSetPicDescHeight(decodeOutputDesc_, inputHeight_);
    acldvppSetPicDescWidthStride(decodeOutputDesc_, decodeOutWidthStride);
    acldvppSetPicDescHeightStride(decodeOutputDesc_, decodeOutHeightStride);
    acldvppSetPicDescSize(decodeOutputDesc_, testPic.jpegDecodeSize);

    // 6. 异步解码
    ret = acldvppJpegDecodeAsync(dvppChannelDesc_, inDevBuffer_, inDevBufferSize_, decodeOutputDesc_, stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acldvppJpegDecodeAsync failed, ret = %d", ret);
        return FAILED;
    }

    // 7. 由于使用了异步，则需要使用 stream 来同步信息
    ret = aclrtSynchronizeStream(stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSynchronizeStream failed");
        return FAILED;
    }

    // 8. 获取输出图片信息并保存至本地
    decodeDataSize_ = acldvppGetPicDescSize(decodeOutputDesc_);

    (void)acldvppFree(picDevBuffer);
    picDevBuffer = nullptr;

    int dir_tail_index = image_path.find("/data");
    std::string outfile_dir = image_path.substr(0, dir_tail_index) + "/" + "out/output/";
    std::string outfile_path = outfile_dir + image_path.substr(dir_tail_index+5+1, image_path.rfind(".jpg")-dir_tail_index-5-1)
    + "_jpegd_" + std::to_string(testPic.width) + "_" + std::to_string(testPic.height) + ".yuv";
    INFO_LOG("outfile_path=%s", outfile_path.c_str());


    ret = SaveDvppOutputData(outfile_path.c_str(), decodeOutDevBuffer_, decodeDataSize_);
    if (ret != SUCCESS) {
        ERROR_LOG("save dvpp output data failed");
        return FAILED;
    }

    return SUCCESS;
}
