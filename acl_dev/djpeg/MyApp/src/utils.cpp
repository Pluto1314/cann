#include "utils.h"
#include <ios>

aclrtRunMode Utils::runMode = ACL_HOST;

Utils::Utils(): deviceId_(0), context_(nullptr), stream_(nullptr) {}

Utils::~Utils() {
    DestroyResource();
}

void Utils::InitResource(const char *path) {
    aclError ret;

    // 1. acl init
    ret = aclInit(path);
    if (ret != ACL_SUCCESS)
        ERROR_LOG("acl init failed!");
    else
        INFO_LOG("acl init success!");

    // 2. init device
    ret = aclrtSetDevice(deviceId_);
    if (ret != ACL_SUCCESS)
        ERROR_LOG("device init failed!");
    else
        INFO_LOG("device %d init success!", deviceId_);

    // 3. init context
    ret = aclrtCreateContext(&context_, deviceId_);
    if (ret != ACL_SUCCESS)
        ERROR_LOG("context init failed!");
    else
        INFO_LOG("context init success!");

    // 4. init stream
    ret = aclrtCreateStream(&stream_);
    if (ret != ACL_SUCCESS)
        ERROR_LOG("stream init failed!");
    else
        INFO_LOG("stream init success!");

    // 5. get runMode
    ret = aclrtGetRunMode(&runMode);
    if (ret != ACL_SUCCESS)
        ERROR_LOG("get runMode failed!");
    else
        INFO_LOG("get runMode success!");
}

void Utils::DestroyResource() {
    aclError ret;

    // 1. destroy stream
    if (stream_ != nullptr) {
        ret = aclrtDestroyStream(stream_);
        if (ret != ACL_SUCCESS)
            ERROR_LOG("destroy stream failed!");
        stream_ = nullptr;
    }
    INFO_LOG("end to destroy stream!");

    // 2. destroy context
    if (context_ != nullptr) {
        ret = aclrtDestroyContext(context_);
        if (ret != ACL_SUCCESS) {
            ERROR_LOG("destroy context failed!");
        }
        context_ = nullptr;
    }
    INFO_LOG("end to destroy context!");

    // 3. destroy device
    ret = aclrtResetDevice(deviceId_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("reset device failed!");
    }
    INFO_LOG("end to reset device is %d!", deviceId_);

    // 4. destroy acl
    ret = aclFinalize();
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("finalize acl failed!");
    }
    INFO_LOG("end to finalize acl!");
}

bool Utils::GetRunMode() {
    if (runMode == ACL_HOST)
        return true;
    return false;
}

aclrtStream& Utils::StreamGetter() {
    return stream_;
}