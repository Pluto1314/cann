/**
* @file Main.cpp
*
* Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <cstdint>
#include <cstdio>
#include <iostream>
#include "acl/acl.h"
#include <fstream>
#include <dirent.h>
#include <cstring>
#include <new>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
#include <wchar.h>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"

#include "decode.h"
#include "utils.h"

using namespace std;

int main(int argc, char* argv[])
{
    // 1. acl init
    Utils myUtils;
    const char *aclConfigPath = "../src/acl.json";
    myUtils.InitResource(aclConfigPath);

    // 2. 准备图片信息
    string image_path = "../data/bb_V0010_I0002160.jpg";
    uint32_t image_width = 0;
    uint32_t image_height = 0;
    PicDesc testPic_ = {image_path, image_width, image_height};
    INFO_LOG("start to process picture: %s", testPic_.picName.c_str());

    // 3. 开始解码
    Decode myDecode;
    myDecode.StartDecode(testPic_, image_path, myUtils.StreamGetter());

    // 4. 关闭申请的资源
    //myDecode.DestroyDecodeResource();
    // myUtils.DestroyResource();
    // 由析构函数自动释放

    INFO_LOG("execute sample success");
    return SUCCESS;
}