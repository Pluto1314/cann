#pragma once

#include <iostream>
#include <cstdint>
#include <string>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#include "utils.h"


class Decode {
public:
    Decode();
    ~Decode();
    void* GetDeviceBufferOfPicture(const PicDesc &picDesc, uint32_t &devPicBufferSize);
    Result SaveDvppOutputData(const char *fileName, const void *devPtr, uint32_t dataSize);
    void SetInput(void *inDevBuffer, int inDevBufferSize, int inputWidth, int inputHeight);
    Result StartDecode(PicDesc &testPic, std::string image_path, aclrtStream &stream_);
    void DestroyDecodeResource();

private:
    acldvppChannelDesc *dvppChannelDesc_;

    // 输出
    void *decodeOutDevBuffer_;
    acldvppPicDesc *decodeOutputDesc_;
    uint32_t decodeDataSize_;

    // 输入
    void *inDevBuffer_;
    uint32_t inDevBufferSize_;

    uint32_t inputWidth_;
    uint32_t inputHeight_;
};

