#pragma once

#include <stdint.h>
#include <string>
#include <iostream>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO] " fmt "\n", ##args)
#define WARN_LOG(fmt, args...) fprintf(stdout, "[WARN] " fmt "\n", ##args)
#define ERROR_LOG(fmt, args...) fprintf(stdout, "[ERROR] " fmt "\n", ##args)


typedef enum Result {
    SUCCESS = 0,
    FAILED = 1
} Result;

typedef struct PicDesc {
    std::string picName;
    uint32_t width;
    uint32_t height;
    uint32_t jpegDecodeSize;
} PicDesc;


class Utils {
public:
    Utils();
    ~Utils();
    void InitResource(const char *path);
    void DestroyResource();
    aclrtStream& StreamGetter();
    static bool GetRunMode();

private:
    int32_t deviceId_;
    aclrtContext context_;
    aclrtStream stream_;
    static aclrtRunMode runMode;
};