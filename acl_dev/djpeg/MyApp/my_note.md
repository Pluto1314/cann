# 遇到的问题
1. Error:/usr/local/Ascend/ascend-toolkit/5.1.RC1.alpha003/x86_64-linux/runtime/include/acl/ops/acl_dvpp.h:15:2: error: #error "if you want to use dvpp funtions ,please use the macro definition (ENABLE_DVPP_INTERFACE)."
   解决办法：在CMakeLists.txt中添加`add_definitions(-DENABLE_DVPP_INTERFACE)`这行申明即可

   原因：使用dvpp需要添加申明

2. 建议使用dvpp时，在CMakeLists.txt添加以下两行：
   ```txt
   add_definitions(-DENABLE_DVPP_INTERFACE)
   
   target_link_libraries(main ascendcl acl_cblas acl_dvpp stdc++)
   ```

   