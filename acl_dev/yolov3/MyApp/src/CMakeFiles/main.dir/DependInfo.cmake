# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/HwHiAiUser/my_project/yolov3/src/dvpp_jpegd.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/dvpp_jpegd.cpp.o"
  "/home/HwHiAiUser/my_project/yolov3/src/dvpp_process.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/dvpp_process.cpp.o"
  "/home/HwHiAiUser/my_project/yolov3/src/dvpp_resize.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/dvpp_resize.cpp.o"
  "/home/HwHiAiUser/my_project/yolov3/src/main.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/main.cpp.o"
  "/home/HwHiAiUser/my_project/yolov3/src/model_process.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/model_process.cpp.o"
  "/home/HwHiAiUser/my_project/yolov3/src/object_detect.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/object_detect.cpp.o"
  "/home/HwHiAiUser/my_project/yolov3/src/utils.cpp" "/home/HwHiAiUser/my_project/yolov3/src/CMakeFiles/main.dir/utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ENABLE_DVPP_INTERFACE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/Ascend/ascend-toolkit/latest/runtime/include"
  "/acllib/include"
  "../inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
