#pragma once
#include <iostream>
#include <unistd.h>
#include <dirent.h>
#include <fstream>
#include <cstring>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <map>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#include <cstdint>
#include <vector>

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO]  " fmt "\n", ##args)
#define WARN_LOG(fmt, args...) fprintf(stdout, "[WARN]  " fmt "\n", ##args)
#define ERROR_LOG(fmt, args...) fprintf(stdout, "[ERROR] " fmt "\n", ##args)

using namespace std;

typedef enum Result {
    SUCCESS = 0,
    FAILED = 1
} Result;

typedef struct PicDesc {
    std::string picName;
    int width;
    int height;
}PicDesc;


void vdecFunc();
void resizeFunc(string image_path, string output_path, int inw, int inh, int outw, int outh);
bool getFileNames(const std::string& dir_in, std::vector<std::string>& files);
void EncodeFunc(string image_path, string outfile_path_, uint32_t image_width, uint32_t image_height);


extern aclrtRunMode runMode;

extern int32_t deviceId_;
extern aclrtContext context_;
extern aclrtStream stream_;
extern pthread_t threadId_;
extern char *outFolder;
extern PicDesc picDesc_;

extern int32_t format_; // 1：YUV420 semi-planner（nv12）; 2：YVU420 semi-planner（nv21）

/* 0：H265 main level
 * 1：H264 baseline level
 * 2：H264 main level
 * 3：H264 high level
 */
extern int32_t enType_;

extern aclvdecChannelDesc *vdecChannelDesc_;
extern acldvppStreamDesc *streamInputDesc_;
extern acldvppPicDesc *picOutputDesc_;
extern acldvppChannelDesc *dvppChannelDesc_;
extern void *picOutBufferDev_;
extern void *inBufferDev_;
extern uint32_t inBufferSize_;
extern bool runFlag;

extern std::string filePath;
extern int inputWidth;
extern int inputHeight;

extern PicDesc inPicDesc;
extern PicDesc outPicDesc;

extern void* decodeOutDevBuffer_; // decode output buffer
extern acldvppPicDesc *decodeOutputDesc_; //decode output desc
extern uint32_t decodeDataSize_;
extern acldvppJpegeConfig *jpegeConfig_;
extern void *inDevBuffer_;  // decode input buffer
extern uint32_t inDevBufferSize_; // dvpp input buffer size
extern uint32_t inputWidth_; // input pic width
extern uint32_t inputHeight_; // input pic height
extern void* encode_out_buffer_dev_; // encode output buffer
extern acldvppPicDesc *encodeInputDesc_; //encode input desf
extern uint32_t in_devbuffer_size_encode_; // input pic size for encode
extern uint32_t encode_outbuffer_size_;