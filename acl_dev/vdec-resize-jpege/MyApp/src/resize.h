#pragma once

#include "main.h"

Result Initparam(std::string image_path, std::string output_path, int inw_, int inh_, int outw_, int outh_);
uint32_t AlignmentHelper(uint32_t origSize, uint32_t alignment);
uint32_t SaveDvppOutputDataResize(const char *fileName, const void *devPtr, uint32_t dataSize);
char* ReadBinFileResize(std::string fileName, uint32_t &fileSize);