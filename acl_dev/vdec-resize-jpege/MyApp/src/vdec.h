#pragma once

#include "main.h"

std::string currentTime();
bool ReadFileToDeviceMem(const char *fileName, void *&dataDev, uint32_t &fileSize);
bool WriteToFile(const char *fileName, const void *dataDev, uint32_t dataSize);
void *ThreadFunc(void *arg);
void callback(acldvppStreamDesc *input, acldvppPicDesc *output, void *userdata);
