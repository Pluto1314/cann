#pragma once

#include "main.h"

uint32_t alignment_helper(uint32_t origSize, uint32_t alignment);
void set_input4_jpege(char &inDevBuffer, int inDevBufferSize, int inputWidth, int inputHeight);
uint32_t compute_encode_inputsize(int inputWidth, int inputHeight);
char* get_picdevbuffer4_jpege(const PicDesc &picDesc, uint32_t &PicBufferSize);
Result save_dvpp_outputdata(const char *fileName, const void *devPtr, uint32_t dataSize);