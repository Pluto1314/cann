/**
* @file Main.cpp
*
* Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <vector>
#include <string.h>
#include "main.h"
#include "vdec.h"
#include "resize.h"
#include "encode.h"
using namespace std;

aclrtRunMode runMode;

int32_t deviceId_;
aclrtContext context_;
aclrtStream stream_;
pthread_t threadId_;
char *outFolder;
PicDesc picDesc_;

int32_t format_ = 1; // 1：YUV420 semi-planner（nv12）; 2：YVU420 semi-planner（nv21）

/* 0：H265 main level
 * 1：H264 baseline level
 * 2：H264 main level
 * 3：H264 high level
 */
int32_t enType_ = 2 ;

aclvdecChannelDesc *vdecChannelDesc_;
acldvppStreamDesc *streamInputDesc_;
acldvppPicDesc *picOutputDesc_;
acldvppChannelDesc *dvppChannelDesc_;
void *picOutBufferDev_;
void *inBufferDev_;
uint32_t inBufferSize_;
bool runFlag = true;

std::string filePath= "";
int inputWidth = 720;
int inputHeight = 1280;

PicDesc inPicDesc;
PicDesc outPicDesc;

void* decodeOutDevBuffer_; // decode output buffer
acldvppPicDesc *decodeOutputDesc_; //decode output desc
uint32_t decodeDataSize_;
acldvppJpegeConfig *jpegeConfig_;
void *inDevBuffer_;  // decode input buffer
uint32_t inDevBufferSize_; // dvpp input buffer size
uint32_t inputWidth_; // input pic width
uint32_t inputHeight_; // input pic height
void* encode_out_buffer_dev_; // encode output buffer
acldvppPicDesc *encodeInputDesc_; //encode input desf
uint32_t in_devbuffer_size_encode_; // input pic size for encode
uint32_t encode_outbuffer_size_;

bool getFileNames(const std::string& dir_in, std::vector<std::string>& files);

void vdecFunc() {
    /* 3. create threadId */
    pthread_create(&threadId_, nullptr, ThreadFunc, nullptr);

    /*4.Set the properties of the channel description information when creating the video code stream
     processing channel, in which the callback callback function needs to be created in advance by the
      user.*/
    vdecChannelDesc_ = aclvdecCreateChannelDesc();

    // channelId: 0-15
    aclvdecSetChannelDescChannelId(vdecChannelDesc_, 10);
    aclvdecSetChannelDescThreadId(vdecChannelDesc_, threadId_);
    /* Sets the callback function*/
    aclvdecSetChannelDescCallback(vdecChannelDesc_, callback);

    //The H265_MAIN_LEVEL video encoding protocol is used in the example
    aclvdecSetChannelDescEnType(vdecChannelDesc_, static_cast<acldvppStreamFormat>(enType_));
    //PIXEL_FORMAT_YVU_SEMIPLANAR_420
    aclvdecSetChannelDescOutPicFormat(vdecChannelDesc_, static_cast<acldvppPixelFormat>(format_));

    /* 5.Create video stream processing channel */
    aclvdecCreateChannel(vdecChannelDesc_);

    /* Video decoding processing */
    //
    // int rest_len = 10;
    void *inBufferDev = nullptr;
    uint32_t inBufferSize = 0;
    size_t outPutDataSize = (inputWidth * inputHeight * 3) / 2;
    streamInputDesc_ = acldvppCreateStreamDesc();

    //
    vector<string> file_name_v;
    string path = "../data";
    getFileNames(path, file_name_v);
    for (int i = 0; i < file_name_v.size(); i++) {
        INFO_LOG("start vdec files: %s", file_name_v[i].c_str());
        // read file to device memory
        //
        ReadFileToDeviceMem(file_name_v[i].c_str(), inBufferDev, inBufferSize);
        // Create input video stream description information, set the properties of the stream information

        //inBufferDev_ means the memory for input video data by Device, and inBufferSize_ means the memory size
        acldvppSetStreamDescData(streamInputDesc_, inBufferDev);
        acldvppSetStreamDescSize(streamInputDesc_, inBufferSize);


        //Device memory picOutBufferDev_ is used to store output data decoded by VDEC
        acldvppMalloc(&picOutBufferDev_, outPutDataSize);
        //Create output image description information, set the image description information properties
        //picOutputDesc_ is acldvppPicDesc
        picOutputDesc_ = acldvppCreatePicDesc();

        acldvppSetPicDescData(picOutputDesc_, picOutBufferDev_);
        acldvppSetPicDescSize(picOutputDesc_, outPutDataSize);
        acldvppSetPicDescFormat(picOutputDesc_, static_cast<acldvppPixelFormat>(format_));


        //
        aclvdecSendFrame(vdecChannelDesc_, streamInputDesc_, picOutputDesc_, nullptr, nullptr);
    }

    // ret = acldvppFree(reinterpret_cast<void *>(picOutBufferDev_));
    acldvppFree(picOutBufferDev_);

    // Release acldvppPicDesc type data, representing output picture description data after decoding
    acldvppDestroyPicDesc(picOutputDesc_);


    acldvppFree(inBufferDev);
    acldvppDestroyStreamDesc(streamInputDesc_);

    aclvdecDestroyChannel(vdecChannelDesc_);
    aclvdecDestroyChannelDesc(vdecChannelDesc_);
    vdecChannelDesc_ = nullptr;

    // destory thread
    runFlag = false;
    void *res = nullptr;
    pthread_join(threadId_, &res);
}

void resizeFunc(string image_path, string output_path, int inw, int inh, int outw, int outh) {
    /* 3.Initialization parameters: width and height of the original image, crop width and height. Initialize folder: Output folder*/
    Initparam(image_path, output_path, inw, inh, outw, outh);
    const int modelInputWidth = outPicDesc.width; // cur model shape is 224 * 224
    const int modelInputHeight = outPicDesc.height;

    /* 4. Channel description information when creating image data processing channels, dvppChannelDesc_ is acldvppChannelDesc type*/
    dvppChannelDesc_ = acldvppCreateChannelDesc();

    /* 5. Create the image data processing channel.*/
    acldvppCreateChannel(dvppChannelDesc_);

    // GetPicDevBuffer4JpegD
    uint32_t inputBuffSize = 0;
    char* inputBuff = ReadBinFileResize(inPicDesc.picName, inputBuffSize);
    void *inBufferDev = nullptr;
    inBufferSize_ = inputBuffSize;
    acldvppMalloc(&inBufferDev, inBufferSize_);
    if (runMode == ACL_HOST) {
        aclrtMemcpy(inBufferDev, inBufferSize_, inputBuff, inputBuffSize, ACL_MEMCPY_HOST_TO_DEVICE);
    }
    else {
        aclrtMemcpy(inBufferDev, inBufferSize_, inputBuff, inputBuffSize, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }
    delete[] inputBuff;

    acldvppResizeConfig *resizeConfig_ = acldvppCreateResizeConfig();

    /* processdecode*/
    inputWidth = inPicDesc.width;
    inputHeight = inPicDesc.height;
    uint32_t sizeAlignment = 3;
    uint32_t sizeNum = 2;
    // if the input yuv is from JPEGD, it should be aligned to 128*16
    // if the input yuv is from VDEC, it shoud be aligned to 16*2
    uint32_t inputWidthStride = AlignmentHelper(inputWidth, 128);
    uint32_t inputHeightStride = AlignmentHelper(inputHeight, 16);
    uint32_t inputBufferSize = inputWidthStride * inputHeightStride * sizeAlignment / sizeNum;
    acldvppPicDesc *vpcInputDesc_ = acldvppCreatePicDesc();
    acldvppPicDesc *vpcOutputDesc_ = acldvppCreatePicDesc();
    void *vpcOutBufferDev_ = nullptr;
    acldvppSetPicDescData(vpcInputDesc_, reinterpret_cast<char *>(inBufferDev));
    // the format is the input yuv's format.
    acldvppSetPicDescFormat(vpcInputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(vpcInputDesc_, inputWidth);
    acldvppSetPicDescHeight(vpcInputDesc_, inputHeight);
    acldvppSetPicDescWidthStride(vpcInputDesc_, inputWidthStride);
    acldvppSetPicDescHeightStride(vpcInputDesc_, inputHeightStride);
    acldvppSetPicDescSize(vpcInputDesc_, inputBufferSize);

    // here is the VPC constraints, should be aligned to 16*2
    int resizeOutWidthStride = AlignmentHelper(modelInputWidth, 16);
    int resizeOutHeightStride = AlignmentHelper(modelInputHeight, 2);
    uint32_t vpcOutBufferSize_ = resizeOutWidthStride * resizeOutHeightStride * sizeAlignment / sizeNum;
    acldvppMalloc(&vpcOutBufferDev_, vpcOutBufferSize_);
    acldvppSetPicDescData(vpcOutputDesc_, vpcOutBufferDev_);
    // the format should be under the VPC constraints(only support the following 2):
    // PIXEL_FORMAT_YUV_SEMIPLANAR_420 = 1
    // PIXEL_FORMAT_YVU_SEMIPLANAR_420 = 2
    acldvppSetPicDescFormat(vpcOutputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(vpcOutputDesc_, modelInputWidth);
    acldvppSetPicDescHeight(vpcOutputDesc_, modelInputHeight);
    acldvppSetPicDescWidthStride(vpcOutputDesc_, resizeOutWidthStride);
    acldvppSetPicDescHeightStride(vpcOutputDesc_, resizeOutHeightStride);
    acldvppSetPicDescSize(vpcOutputDesc_, vpcOutBufferSize_);

    aclError ret = acldvppVpcResizeAsync(dvppChannelDesc_, vpcInputDesc_,
    vpcOutputDesc_, resizeConfig_, stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acldvppVpcResizeAsync failed, ret = %d", ret);
        //return FAILED;
    }

    aclrtSynchronizeStream(stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSynchronizeStream failed");
        //return FAILED;
    }

    /* DestroyResizeResource*/
    (void)acldvppDestroyResizeConfig(resizeConfig_);
    resizeConfig_ = nullptr;
    (void)acldvppDestroyPicDesc(vpcInputDesc_);
    vpcInputDesc_ = nullptr;
    (void)acldvppDestroyPicDesc(vpcOutputDesc_);
    vpcOutputDesc_ = nullptr;

    (void)acldvppFree(inBufferDev);
    inBufferDev = nullptr;

    SaveDvppOutputDataResize(outPicDesc.picName.c_str(), vpcOutBufferDev_, vpcOutBufferSize_);
    if (vpcOutBufferDev_ != nullptr) {
        (void)acldvppFree(vpcOutBufferDev_);
        vpcOutBufferDev_ = nullptr;
    }
    (void)acldvppDestroyChannel(dvppChannelDesc_);
    (void)acldvppDestroyChannelDesc(dvppChannelDesc_);
    dvppChannelDesc_ = nullptr;
}

void EncodeFunc(string image_path, string outfile_path_, uint32_t image_width, uint32_t image_height) {

    uint32_t encodeLevel = 100; // default optimal level (0-100)
    PicDesc testPic = {image_path, image_width, image_height};
    INFO_LOG("Start to process picture: %s", testPic.picName.c_str());
    INFO_LOG("Call JpegE");

    //    DIR *dir;
    //    if ((dir = opendir("./output")) == NULL){
    //        system("mkdir ./output");
    //    }

    //create dvpp channel
    dvppChannelDesc_ = acldvppCreateChannelDesc();
    INFO_LOG("Call acldvppCreateChannelDesc success");
    acldvppCreateChannel(dvppChannelDesc_);
    INFO_LOG("Call acldvppCreateChannel success");
    INFO_LOG("DVPP init resource success");

    uint32_t jpegInBufferSize;
    jpegInBufferSize = compute_encode_inputsize(testPic.width, testPic.height);
    //input mem malloc
    char* picDevBuffer = get_picdevbuffer4_jpege(testPic, jpegInBufferSize);
    if (nullptr == picDevBuffer) {
        ERROR_LOG("get picDevBuffer failed, index is %d", 0);
        //return FAILED;
    }
    set_input4_jpege(*picDevBuffer, jpegInBufferSize, testPic.width, testPic.height);
    //create pic desc & set pic attrs
    uint32_t widthAlignment = 16;
    uint32_t heightAlignment = 2;
    uint32_t encodeInWidthStride = alignment_helper(inputWidth_, widthAlignment);
    uint32_t encodeInHeightStride = alignment_helper(inputHeight_, heightAlignment);
    if (encodeInWidthStride == 0 || encodeInHeightStride == 0) {
        ERROR_LOG("InitEncodeInputDesc AlignmentHelper failed");
        //return FAILED;
    }
    encodeInputDesc_ = acldvppCreatePicDesc();
    INFO_LOG("Call acldvppCreatePicDesc success");
    if (encodeInputDesc_ == nullptr) {
        ERROR_LOG("acldvppCreatePicDesc encodeInputDesc_ failed");
        //return FAILED;
    }

    acldvppSetPicDescData(encodeInputDesc_, reinterpret_cast<void *>(inDevBuffer_));
    acldvppSetPicDescFormat(encodeInputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(encodeInputDesc_, inputWidth_);
    acldvppSetPicDescHeight(encodeInputDesc_, inputHeight_);
    acldvppSetPicDescWidthStride(encodeInputDesc_, encodeInWidthStride);
    acldvppSetPicDescHeightStride(encodeInputDesc_, encodeInHeightStride);
    acldvppSetPicDescSize(encodeInputDesc_, in_devbuffer_size_encode_);

    jpegeConfig_ = acldvppCreateJpegeConfig();
    INFO_LOG("Call acldvppCreateJpegeConfig success");
    acldvppSetJpegeConfigLevel(jpegeConfig_, encodeLevel);

    //output mem malloc
    acldvppJpegPredictEncSize(encodeInputDesc_, jpegeConfig_, &encode_outbuffer_size_);
    aclError aclRet = acldvppMalloc(&encode_out_buffer_dev_, encode_outbuffer_size_);

    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("malloc encodeOutBufferDev_ failed, aclRet is %d", aclRet);
        //return FAILED;
    }

    //call Asynchronous api
    aclRet = acldvppJpegEncodeAsync(dvppChannelDesc_, encodeInputDesc_, encode_out_buffer_dev_,
    &encode_outbuffer_size_, jpegeConfig_, stream_);
    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("acldvppJpegEncodeAsync failed, aclRet = %d", aclRet);
        //return FAILED;
    }
    INFO_LOG("Call acldvppJpegEncodeAsync success");
    aclRet = aclrtSynchronizeStream(stream_);
    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("encode aclrtSynchronizeStream failed, aclRet = %d", aclRet);
        //return FAILED;
    }

    //malloc host mem & save pic

    //    int dir_tail_index = image_path.find("/data");
    //    std::string outfile_dir = image_path.substr(0, dir_tail_index) + "/" + "out/output/";
    //    std::string outfile_path = outfile_dir + image_path.substr(dir_tail_index+5+1, image_path.rfind(".yuv")-dir_tail_index-5-1)
    //    + "_jpege_" + std::to_string(inputWidth_) + "_" + std::to_string(inputHeight_) + ".jpg";
    std::string outfile_path = outfile_path_;
    // "../out/output/resize_224_224.jpg";
    // INFO_LOG("Encode outfile_path=%s", outfile_path.c_str());

    // std::string encodeOutFileName = image_path.replace(image_path.rfind(".yuv"), 4, "_jpege_output");
    // encodeOutFileName = encodeOutFileName + ".jpg";
    Result ret = save_dvpp_outputdata(outfile_path.c_str(), encode_out_buffer_dev_, encode_outbuffer_size_);
    if (ret != SUCCESS) {
        ERROR_LOG("save encode output data failed.");
        //return FAILED;
    }

    if (jpegeConfig_ != nullptr) {
        (void)acldvppDestroyJpegeConfig(jpegeConfig_);
        jpegeConfig_ = nullptr;
    }
    INFO_LOG("Call acldvppDestroyJpegeConfig success");
    if (encodeInputDesc_ != nullptr) {
        (void)acldvppDestroyPicDesc(encodeInputDesc_);
        encodeInputDesc_ = nullptr;
    }
    INFO_LOG("Call acldvppDestroyPicDesc success");
    if (inDevBuffer_ != nullptr) {
        (void)acldvppFree(inDevBuffer_);
        inDevBuffer_ = nullptr;
    }
    INFO_LOG("Call acldvppFree success");

    //    string out_name = image_path.substr(dir_tail_index+5+1, image_path.rfind(".yuv")-dir_tail_index-5-1)
    //    + "_jpege_" + std::to_string(inputWidth_) + "_" + std::to_string(inputHeight_) + ".jpg";
    //    return out_name;
}

bool getFileNames(const std::string& dir_in, std::vector<std::string>& files) {
    if (dir_in.empty()) {
        return false;
    }
    struct stat s;
    stat(dir_in.c_str(), &s);
    if (!S_ISDIR(s.st_mode)) {
        return false;
    }
    DIR* open_dir = opendir(dir_in.c_str());
    if (NULL == open_dir) {
        std::exit(EXIT_FAILURE);
    }
    dirent* p = nullptr;
    while( (p = readdir(open_dir)) != nullptr) {
        struct stat st;
        if (p->d_name[0] != '.') {
            //因为是使用devC++ 获取windows下的文件，所以使用了 "\" ,linux下要换成"/"
            std::string name = dir_in + std::string("/") + std::string(p->d_name);
            stat(name.c_str(), &st);
            if (S_ISDIR(st.st_mode)) {
                getFileNames(name, files);
            }
            else if (S_ISREG(st.st_mode)) {
                files.push_back(name);
            }
        }
    }
    closedir(open_dir);
    return true;
}

int main(int argc, char* argv[])
{

    DIR *dir;
    if ((dir = opendir("./output")) == NULL)
        system("mkdir ./output");

    /* 1. ACL initialization */
    const char *aclConfigPath = "../acl.json";
    aclInit(aclConfigPath);

    /* 2. Run the management resource application, including Device, Context, Stream */
    aclrtSetDevice(deviceId_);
    aclrtCreateContext(&context_, deviceId_);
    // ret = aclrtCreateStream(&stream_);

    // aclrtStream stream1;
    // ret = aclrtCreateStream(&stream1);
    aclrtGetRunMode(&runMode);

    // 视频解码
    vdecFunc();

    // 缩放
    vector<string> file_name;
    string path = "../out/output/vdec";
    getFileNames(path, file_name);
    for(int i = 0; i <file_name.size(); i++)
    {
        INFO_LOG("start resize files: %s", file_name[i].c_str());
        string outName = "../out/output/resize/" +
                        file_name[i].substr(19).substr(0, file_name[i].substr(19).find(".")) +
                        "_resize_224_224.yuv";
        resizeFunc(file_name[i], outName, inputWidth, inputHeight, 224, 224);
        INFO_LOG("output resize files: %s", outName.c_str());
    }

    // 图片编码
    vector<string> file_name_r;
    path = "../out/output/resize";
    getFileNames(path, file_name_r);
    for(int i = 0; i <file_name_r.size(); i++)
    {
        INFO_LOG("start jpege files: %s", file_name_r[i].c_str());
        string outName = "../out/output/jpege/" +
                        file_name_r[i].substr(21).substr(0, file_name_r[i].substr(21).find(".")) +
                        "_jpege.jpg";
        EncodeFunc(file_name_r[i], outName, 224, 224);
        INFO_LOG("output jpege files: %s", outName.c_str());
    }

    // 释放资源
    aclrtDestroyContext(context_);
    context_ = nullptr;
    aclrtResetDevice(deviceId_);
    aclFinalize();

    return SUCCESS;
}