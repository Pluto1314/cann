#pragma once
#include <iostream>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <fstream>
#include <cstring>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <map>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#include <cstdint>
#include "opencv2/opencv.hpp"

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO]  " fmt "\n", ##args)
#define WARN_LOG(fmt, args...) fprintf(stdout, "[WARN]  " fmt "\n", ##args)
#define ERROR_LOG(fmt, args...) fprintf(stdout, "[ERROR]  " fmt "\n", ##args)

typedef struct PicDesc {
    std::string picName;
    int width;
    int height;
}PicDesc;

typedef enum Result {
    SUCCESS = 0,
    FAILED = 1
}Result;

bool getFileNames(const std::string& dir_in, std::vector<std::string>& files);

extern int32_t deviceId;
extern aclrtContext context;
extern aclrtStream stream;
extern aclrtRunMode runMode;
extern pthread_t threadId;
extern pthread_t threadId_;

extern aclvencChannelDesc *vencChannelDesc;
extern acldvppPicDesc *vpcInputDesc;
extern aclvencFrameConfig *vencFrameConfig;
extern acldvppStreamDesc *outputStreamDesc;
extern void *codeInputBufferDev;
extern acldvppPixelFormat format;
extern int32_t enType;
extern uint32_t inputBufferSize;
extern FILE *outFileFp;
extern bool runFlag;

extern int32_t format_;
extern std::string filePath;
extern int inputWidth_;
extern int inputHeight_;
extern char *outFolder;
extern PicDesc picDesc_;
extern aclvdecChannelDesc *vdecChannelDesc_;
extern acldvppStreamDesc *streamInputDesc_;
extern acldvppPicDesc *picOutputDesc_;
extern void *picOutBufferDev_;
extern void *inBufferDev_;
extern uint32_t inBufferSize_;