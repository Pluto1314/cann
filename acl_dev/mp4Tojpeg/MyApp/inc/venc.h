#include "utils.h"

#define RGBU8_IMAGE_SIZE(width, height) ((width) * (height) * 3)
#define YUV420SP_SIZE(width, height) ((width) * (height) * 3 / 2)

#define ALIGN_UP(num, align) (((num) + (align) - 1) & ~((align) - 1))
#define ALIGN_UP2(num) ALIGN_UP(num, 2)
#define ALIGN_UP16(num) ALIGN_UP(num, 16)
#define ALIGN_UP128(num) ALIGN_UP(num, 128)

void *ThreadFunc(aclrtContext sharedContext);
bool WriteToFile(FILE *outFileFp_, const void *dataDev, uint32_t dataSize);
void callback(acldvppPicDesc *input, acldvppStreamDesc *outputStreamDesc, void *userdata);
Result Init(int imgWidth, int imgHeight);
Result Process(cv::Mat& srcImage);
void VencDestroyResource();