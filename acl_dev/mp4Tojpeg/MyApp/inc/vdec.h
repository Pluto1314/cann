#include "utils.h"

std::string currentTime();
bool ReadFileToDeviceMem(const char *fileName, void *&dataDev, uint32_t &fileSize);
bool WriteToFile_(const char *fileName, const void *dataDev, uint32_t dataSize);
void *ThreadFunc_(void *arg);
void callback_(acldvppStreamDesc *input, acldvppPicDesc *output, void *userdata);