#include "utils.h"
#include "venc.h"

void *ThreadFunc(aclrtContext sharedContext)
{
    if (sharedContext == nullptr) {
        ERROR_LOG("sharedContext can not be nullptr");
        return ((void*)(-1));
    }
    INFO_LOG("use shared context for this thread");
    aclError ret = aclrtSetCurrentContext(sharedContext);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSetCurrentContext failed, errorCode = %d", static_cast<int32_t>(ret));
        return ((void*)(-1));
    }
    INFO_LOG("process callback thread start ");
    while (runFlag) {
        // Notice: timeout 1000ms
        (void)aclrtProcessReport(1000);
        //pthread_testcancel();
    }
    return (void*)0;
}

bool WriteToFile(FILE *outFileFp_, const void *dataDev, uint32_t dataSize)
{
    bool ret = true;
    size_t writeRet = fwrite(dataDev, 1, dataSize, outFileFp_);
    if (writeRet != dataSize) {
        ret = false;
    }
    fflush(outFileFp_);

    return ret;
}

void callback(acldvppPicDesc *input, acldvppStreamDesc *outputStreamDesc, void *userdata)
{
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    void *outputDev = acldvppGetStreamDescData(outputStreamDesc);
    uint32_t streamDescSize = acldvppGetStreamDescSize(outputStreamDesc);
    bool ret;

    static int count = 0;
    FILE *myOutFileFp;

    if (runMode == ACL_HOST) {
        // test 10 pic
        if (count >= 10)
            return;
        void * hostPtr = nullptr;
        aclrtMallocHost(&hostPtr, streamDescSize);
        aclrtMemcpy(hostPtr, streamDescSize, outputDev, streamDescSize, ACL_MEMCPY_DEVICE_TO_HOST);

        std::string myOutputPath = "./output/venc/out_video_" + std::to_string(count) + ".h264";
        myOutFileFp = fopen(myOutputPath.c_str(), "ab");
        if(myOutFileFp == nullptr)    {
            ERROR_LOG("Failed to open  file %s.", myOutputPath.c_str());
            return;
        }
        ret = WriteToFile(myOutFileFp, hostPtr, streamDescSize);
        fclose(myOutFileFp);

        // ret = WriteToFile(outFileFp, hostPtr, streamDescSize);
        (void)aclrtFreeHost(hostPtr);
    }
    else{
        ret = WriteToFile(outFileFp, outputDev, streamDescSize);
    }

    if (!ret) {
        ERROR_LOG("write file failed.");
    }
    INFO_LOG("success to callback, stream size:%u", streamDescSize);
    count++;
}

Result Init(int imgWidth, int imgHeight)
{
    pthread_create(&threadId, nullptr, ThreadFunc, context);
    int width = imgWidth;
    int height = imgHeight;
    uint32_t alignWidth = ALIGN_UP128(width);
    uint32_t alignHeight = ALIGN_UP16(height);
    if (alignWidth == 0 || alignHeight == 0) {
        ERROR_LOG("InitCodeInputDesc AlignmentHelper failed. image w %d, h %d, align w%u, h%u",
        width, height, alignWidth, alignHeight);
        return FAILED;
    }
    //Allocate a large enough memory
    inputBufferSize = YUV420SP_SIZE(alignWidth, alignHeight);
    aclError ret = acldvppMalloc(&codeInputBufferDev, inputBufferSize);

    format = static_cast<acldvppPixelFormat>(PIXEL_FORMAT_YVU_SEMIPLANAR_420);
    vencFrameConfig = aclvencCreateFrameConfig();
    aclvencSetFrameConfigForceIFrame(vencFrameConfig, 0);
    if (vencFrameConfig == nullptr) {
        ERROR_LOG("Dvpp init failed for create config failed");
        return FAILED;
    }

    vencChannelDesc = aclvencCreateChannelDesc();
    if (vencChannelDesc == nullptr) {
        ERROR_LOG("aclvencCreateChannelDesc failed");
        return FAILED;
    }
    ret = aclvencSetChannelDescThreadId(vencChannelDesc, threadId);
    ret = aclvencSetChannelDescCallback(vencChannelDesc, callback);
    ret = aclvencSetChannelDescEnType(vencChannelDesc, static_cast<acldvppStreamFormat>(enType));
    ret = aclvencSetChannelDescPicFormat(vencChannelDesc, format);
    ret = aclvencSetChannelDescKeyFrameInterval(vencChannelDesc, 1);
    ret = aclvencSetChannelDescPicWidth(vencChannelDesc, width);
    ret = aclvencSetChannelDescPicHeight(vencChannelDesc, height);
    ret = aclvencCreateChannel(vencChannelDesc);

    vpcInputDesc = acldvppCreatePicDesc();
    if (vpcInputDesc == nullptr) {
        ERROR_LOG("acldvppCreatePicDesc vpcInputDesc failed");
        return FAILED;
    }
    ret = acldvppSetPicDescFormat(vpcInputDesc, format);
    ret = acldvppSetPicDescWidth(vpcInputDesc, width);
    ret = acldvppSetPicDescHeight(vpcInputDesc, height);
    ret = acldvppSetPicDescWidthStride(vpcInputDesc, alignWidth);
    ret = acldvppSetPicDescHeightStride(vpcInputDesc, alignHeight);
    INFO_LOG("dvpp init resource ok");
    return SUCCESS;
}

Result Process(cv::Mat& srcImage)
{
    aclError ret;
    if(runMode == ACL_HOST) {
        ret = aclrtMemcpy(codeInputBufferDev, inputBufferSize, srcImage.data, inputBufferSize, ACL_MEMCPY_HOST_TO_DEVICE);
    }
    else {
        ret = aclrtMemcpy(codeInputBufferDev, inputBufferSize, srcImage.data, inputBufferSize, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }

    ret = acldvppSetPicDescData(vpcInputDesc, codeInputBufferDev);
    ret = acldvppSetPicDescSize(vpcInputDesc, inputBufferSize);

    ret = aclvencSendFrame(vencChannelDesc, vpcInputDesc,
    static_cast<void *>(outputStreamDesc), vencFrameConfig, nullptr);
    return SUCCESS;
}

void VencDestroyResource()
{

    aclvencSetFrameConfigEos(vencFrameConfig, 1);
    aclvencSetFrameConfigForceIFrame(vencFrameConfig, 0);
    aclvencSendFrame(vencChannelDesc, nullptr, nullptr, vencFrameConfig, nullptr);

    if (vencFrameConfig != nullptr) {
        (void)aclvencDestroyFrameConfig(vencFrameConfig);
        vencFrameConfig = nullptr;
    }

    if (vpcInputDesc != nullptr) {
        (void)acldvppDestroyPicDesc(vpcInputDesc);
        vpcInputDesc = nullptr;
    }

    if (codeInputBufferDev != nullptr) {
        (void)acldvppFree(codeInputBufferDev);
        codeInputBufferDev = nullptr;
    }

    if (outputStreamDesc != nullptr) {
        (void)acldvppDestroyStreamDesc(outputStreamDesc);
        outputStreamDesc = nullptr;
    }

    aclError aclRet;
    if (vencChannelDesc != nullptr) {
        aclRet = aclvencDestroyChannel(vencChannelDesc);
        if (aclRet != ACL_SUCCESS) {
            ERROR_LOG("aclvencDestroyChannel failed, aclRet = %d", aclRet);
        }
        (void)aclvencDestroyChannelDesc(vencChannelDesc);
        vencChannelDesc = nullptr;
    }

    runFlag = false;
    void *res = nullptr;
    pthread_cancel(threadId);
    pthread_join(threadId, &res);
    fclose(outFileFp);
    INFO_LOG("end to destroy Resource");
}