/**
* @file Main.cpp
*
* Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <iostream>
#include <string>
#include "utils.h"
#include "venc.h"
#include "vdec.h"

using namespace std;

int32_t deviceId;
aclrtContext context;
aclrtStream stream;
aclrtRunMode runMode;
pthread_t threadId;
pthread_t threadId_;

aclvencChannelDesc *vencChannelDesc = nullptr;
acldvppPicDesc *vpcInputDesc = nullptr;
aclvencFrameConfig *vencFrameConfig = nullptr;
acldvppStreamDesc *outputStreamDesc = nullptr;
void *codeInputBufferDev = nullptr;
acldvppPixelFormat format;
/* 0：H265 main level
 * 1：H264 baseline level
 * 2：H264 main level
 * 3：H264 high level
 */
int32_t enType = 2;
uint32_t inputBufferSize;
FILE *outFileFp;
bool runFlag= true;

int32_t format_ = 1; // 1：YUV420 semi-planner（nv12）; 2：YVU420 semi-planner（nv21）
std::string filePath;
int inputWidth_;
int inputHeight_;
char *outFolder;
PicDesc picDesc_;
aclvdecChannelDesc *vdecChannelDesc_;
acldvppStreamDesc *streamInputDesc_;
acldvppPicDesc *picOutputDesc_;
void *picOutBufferDev_;
void *inBufferDev_;
uint32_t inBufferSize_;

void DestroyAclResource();
Result InitResource();

void vencFunc() {
    //Use Opencv to open the video file
    string videoFile = "../data/detection.mp4";
    printf("open %s\n", videoFile.c_str());
    cv::VideoCapture capture(videoFile);
    if (!capture.isOpened()) {
        cout << "Movie open Error" << endl;
        return;
    }
    cout << "Movie open success" << endl;

    int imgHeight = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
    int imgWidth = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
    INFO_LOG("detection.mp4 W & H : %d & %d", imgWidth, imgHeight);
    inputWidth_ = imgWidth;
    inputHeight_ = imgHeight;

    //open the output file
    string outputPath = "./output/venc/out_video.h264";
    outFileFp = fopen(outputPath.c_str(), "ab");
    if(outFileFp == nullptr)    {
        ERROR_LOG("Failed to open  file %s.", outputPath.c_str());
        return;
    }

    Init(imgWidth, imgHeight);
    cv::Mat frame;
    while(1) {
        //Read a frame of an image
        if (!capture.read(frame)) {
            INFO_LOG("Video capture return false");
            break;
        }
        int cols = frame.cols;
        int rows = frame.rows;
        int Yindex = 0;
        int UVindex = rows * cols;
        cv::Mat NV21(rows+rows/2, cols, CV_8UC1);
        int UVRow{ 0 };
        for (int i=0;i<rows;i++)
        {
            for (int j=0;j<cols;j++)
            {
                uchar* YPointer = NV21.ptr<uchar>(i);
                int B = frame.at<cv::Vec3b>(i, j)[0];
                int G = frame.at<cv::Vec3b>(i, j)[1];
                int R = frame.at<cv::Vec3b>(i, j)[2];
                int Y = (77 * R + 150 * G + 29 * B) >> 8;
                YPointer[j] = Y;
                uchar* UVPointer = NV21.ptr<uchar>(rows+i/2);
                if (i%2==0&&(j)%2==0)
                {
                    int U = ((-44 * R - 87 * G + 131 * B) >> 8) + 128;
                    int V = ((131 * R - 110 * G - 21 * B) >> 8) + 128;
                    UVPointer[j] = V;
                    UVPointer[j+1] = U;
                }
            }
        }
        Process(NV21);
    }
    VencDestroyResource();
    INFO_LOG("Execute video object detection success");
}

void vdecFunc() {
    runFlag = true;
    aclError ret;

    /* 3. create threadId */
    pthread_create(&threadId_, nullptr, ThreadFunc_, nullptr);

    /*4.Set the properties of the channel description information when creating the video code stream
     processing channel, in which the callback callback function needs to be created in advance by the
      user.*/
    vdecChannelDesc_ = aclvdecCreateChannelDesc();

    // channelId: 0-15
    ret = aclvdecSetChannelDescChannelId(vdecChannelDesc_, 10);
    ret = aclvdecSetChannelDescThreadId(vdecChannelDesc_, threadId_);
    /* Sets the callback function*/
    ret = aclvdecSetChannelDescCallback(vdecChannelDesc_, callback_);

    //The H265_MAIN_LEVEL video encoding protocol is used in the example
    ret = aclvdecSetChannelDescEnType(vdecChannelDesc_, static_cast<acldvppStreamFormat>(enType));
    //PIXEL_FORMAT_YVU_SEMIPLANAR_420
    ret = aclvdecSetChannelDescOutPicFormat(vdecChannelDesc_, static_cast<acldvppPixelFormat>(format_));

    /* 5.Create video stream processing channel */
    ret = aclvdecCreateChannel(vdecChannelDesc_);

    /* Video decoding processing */
    int rest_len = 0;
    int max_len = 10;
    void *inBufferDev = nullptr;
    uint32_t inBufferSize = 0;
    size_t outPutDataSize = (inputWidth_ * inputHeight_ * 3) / 2;

    // vector<string> file_name_v;
    string video_path = "../data/out_video_";
    while (rest_len < max_len) {
        // read file to device memory
        string path = video_path + std::to_string(rest_len) + ".h264";
        INFO_LOG("start to process: %s", path.c_str());
        ReadFileToDeviceMem(path.c_str(), inBufferDev, inBufferSize);
        // Create input video stream description information, set the properties of the stream information
        streamInputDesc_ = acldvppCreateStreamDesc();
        //inBufferDev_ means the memory for input video data by Device, and inBufferSize_ means the memory size
        ret = acldvppSetStreamDescData(streamInputDesc_, inBufferDev);
        ret = acldvppSetStreamDescSize(streamInputDesc_, inBufferSize);

        //Device memory picOutBufferDev_ is used to store output data decoded by VDEC
        ret = acldvppMalloc(&picOutBufferDev_, outPutDataSize);

        //Create output image description information, set the image description information properties
        //picOutputDesc_ is acldvppPicDesc
        picOutputDesc_ = acldvppCreatePicDesc();

        ret = acldvppSetPicDescData(picOutputDesc_, picOutBufferDev_);
        ret = acldvppSetPicDescSize(picOutputDesc_, outPutDataSize);
        ret = acldvppSetPicDescFormat(picOutputDesc_, static_cast<acldvppPixelFormat>(format_));
        //
        aclvdecSendFrame(vdecChannelDesc_, streamInputDesc_, picOutputDesc_, nullptr, nullptr);
        rest_len++;
    }

    // ret = acldvppFree(reinterpret_cast<void *>(picOutBufferDev_));
    ret = acldvppFree(picOutBufferDev_);

    // Release acldvppPicDesc type data, representing output picture description data after decoding
    ret = acldvppDestroyPicDesc(picOutputDesc_);


    acldvppFree(inBufferDev);
    ret = acldvppDestroyStreamDesc(streamInputDesc_);

    ret = aclvdecDestroyChannel(vdecChannelDesc_);
    ret = aclvdecDestroyChannelDesc(vdecChannelDesc_);
    vdecChannelDesc_ = nullptr;

    // destory thread
    runFlag = false;
    void *res = nullptr;
    pthread_join(threadId_, &res);
}

void DestroyAclResource()
{
    aclError ret;

    if (stream != nullptr) {
        ret = aclrtDestroyStream(stream);
        if (ret != ACL_SUCCESS) {
            ERROR_LOG("destroy stream failed");
        }
        stream = nullptr;
    }
    INFO_LOG("end to destroy stream");

    if (context != nullptr) {
        ret = aclrtDestroyContext(context);
        if (ret != ACL_SUCCESS) {
            ERROR_LOG("destroy context failed");
        }
        context = nullptr;
    }
    INFO_LOG("end to destroy context");

    ret = aclrtResetDevice(deviceId);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("reset device failed");
    }
    INFO_LOG("end to reset device is %d", deviceId);

    ret = aclFinalize();
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("finalize acl failed");
    }
    INFO_LOG("end to finalize acl");
}

Result InitResource()
{
    const char *aclConfigPath = "../src/acl.json";
    aclError ret = aclInit(aclConfigPath);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("Acl init failed");
        return FAILED;
    }
    INFO_LOG("Acl init success");

    ret = aclrtSetDevice(deviceId);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("Acl open device %d failed", deviceId);
        return FAILED;
    }
    INFO_LOG("Open device %d success", deviceId);

    ret = aclrtCreateContext(&context, deviceId);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acl create context failed");
        return FAILED;
    }
    INFO_LOG("create context success");

    ret = aclrtCreateStream(&stream);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acl create stream failed");
        return FAILED;
    }
    INFO_LOG("create stream success");

    //Gets whether the current application is running on host or Device
    ret = aclrtGetRunMode(&runMode);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acl get run mode failed");
        return FAILED;
    }
    return SUCCESS;
}

int main(int argc, char* argv[])
{
    InitResource();
    vencFunc();
    vdecFunc();
    DestroyAclResource();
    return 0;
}