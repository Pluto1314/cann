#include "utils.h"
#include "vdec.h"

std::string currentTime()
{
    char szBuf[256] = { 0 };
    struct timeval tv;
    struct timezone tz;
    struct tm* p = nullptr;

    gettimeofday(&tv, &tz);
    p = localtime(&tv.tv_sec);
    std::string pi = std::to_string(p->tm_year + 1900) + std::to_string(p->tm_mon + 1) + std::to_string(p->tm_mday) \
		     + "_" + std::to_string(p->tm_hour) + "_" + std::to_string(p->tm_min) + "_" + \
		     std::to_string(p->tm_sec) + "_" + std::to_string(tv.tv_usec);
    return pi;
}

bool ReadFileToDeviceMem(const char *fileName, void *&dataDev, uint32_t &fileSize)
{
    // read data from file.
    FILE *fp = fopen(fileName, "rb+");

    fseek(fp, 0, SEEK_END);
    long fileLenLong = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    auto fileLen = static_cast<uint32_t>(fileLenLong);
    void *data = malloc(fileLen);

    size_t readSize = fread(data, 1, fileLen, fp);
    if (readSize < fileLen) {
        free(data);
        fclose(fp);
        return false;
    }

    fileSize = fileLen;
    // Malloc input device memory
    auto aclRet = acldvppMalloc(&dataDev, fileSize);
    // copy input to device memory
    if (runMode == ACL_HOST) {
        aclRet = aclrtMemcpy(dataDev, fileSize, data, fileLen, ACL_MEMCPY_HOST_TO_DEVICE);
    }
    else{
        aclRet = aclrtMemcpy(dataDev, fileSize, data, fileLen, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }
    free(data);
    fclose(fp);
    return true;
}

bool WriteToFile_(const char *fileName, const void *dataDev, uint32_t dataSize)
{
    void *data = malloc(dataSize);
    if (data == nullptr) {
        ERROR_LOG("malloc data buffer failed. dataSize=%u\n", dataSize);
        return false;
    }

    // copy output to memory
    if (runMode == ACL_HOST) {
        auto aclRet = aclrtMemcpy(data, dataSize, dataDev, dataSize, ACL_MEMCPY_DEVICE_TO_HOST);
    }
    else{
        auto aclRet = aclrtMemcpy(data, dataSize, dataDev, dataSize, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }

    FILE *outFileFp = fopen(fileName, "wb+");

    bool ret = true;
    size_t writeRet = fwrite(data, 1, dataSize, outFileFp);
    if (writeRet != dataSize) {
        ret = false;
    }
    free(data);
    fflush(outFileFp);
    fclose(outFileFp);
    return ret;
}

void *ThreadFunc_(void *arg)
{
    // INFO_LOG("in ThreadFunc. %s", printCurrentTime().c_str());
    // Notice: create context for this thread
    int deviceId = 0;
    aclrtContext context = nullptr;
    aclError ret = aclrtCreateContext(&context, deviceId);

    while (runFlag) {
        // Notice: timeout 1000ms
        aclError aclRet = aclrtProcessReport(1000);
        // INFO_LOG("in ThreadFunc. %s", printCurrentTime().c_str());
    }

    ret = aclrtDestroyContext(context);
    return (void*)0;
}

void callback_(acldvppStreamDesc *input, acldvppPicDesc *output, void *userdata)
{
    // INFO_LOG("in callback begin. %s", printCurrentTime().c_str());
    /*Get the output memory decoded by VDEC, call the custom function WriteToFile to write
	the data in the output memory to the file, and then call the acldvppFree interface to release
	 the output memory*/
    void *vdecOutBufferDev = acldvppGetPicDescData(output);
    uint32_t size = acldvppGetPicDescSize(output);
    static int count = 1;
    std::string fileNameSave = "./output/vdec/image" + std::to_string(count) + ".yuv";
    INFO_LOG("write to: %s", fileNameSave.c_str());
    if (!WriteToFile_(fileNameSave.c_str(), vdecOutBufferDev, size)) {
        ERROR_LOG("write file failed.");
    }
    // need to consider if you need to free/destroy, if don't need, you can.
    // aclError ret = acldvppFree(reinterpret_cast<void *>(vdecOutBufferDev));
    // Release acldvppPicDesc type data, representing output picture description data after decoding
    // ret = acldvppDestroyPicDesc(output);

    INFO_LOG("in callback func, handel frame no. %d,  %s", count,  currentTime().c_str());

    count++;

    // INFO_LOG("in callback end. %s", printCurrentTime().c_str());
}