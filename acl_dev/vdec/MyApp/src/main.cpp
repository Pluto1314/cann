/**
* @file Main.cpp
*
* Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <iostream>
#include "main.h"
#include "vdec.h"
using namespace std;

aclrtRunMode runMode;

int32_t deviceId_;
aclrtContext context_;
aclrtStream stream_;
pthread_t threadId_;
char *outFolder;
PicDesc picDesc_;

int32_t format_ = 1; // 1：YUV420 semi-planner（nv12）; 2：YVU420 semi-planner（nv21）

/* 0：H265 main level
 * 1：H264 baseline level
 * 2：H264 main level
 * 3：H264 high level
 */
int32_t enType_ = 2 ;

aclvdecChannelDesc *vdecChannelDesc_;
acldvppStreamDesc *streamInputDesc_;
acldvppPicDesc *picOutputDesc_;
void *picOutBufferDev_;
void *inBufferDev_;
uint32_t inBufferSize_;
bool runFlag = true;

std::string filePath= "../data/venc_video_10.h264";
const int inputWidth = 960;
const int inputHeight = 544;


void vdecFunc() {
    /* 1. ACL initialization */
    const char *aclConfigPath = "../acl.json";
    aclError ret = aclInit(aclConfigPath);

    /* 2. Run the management resource application, including Device, Context, Stream */
    ret = aclrtSetDevice(deviceId_);
    ret = aclrtCreateContext(&context_, deviceId_);
    // ret = aclrtCreateStream(&stream_);

    // aclrtStream stream1;
    // ret = aclrtCreateStream(&stream1);
    aclrtGetRunMode(&runMode);

    /* 3. create threadId */
    pthread_create(&threadId_, nullptr, ThreadFunc, nullptr);

    /*4.Set the properties of the channel description information when creating the video code stream
     processing channel, in which the callback callback function needs to be created in advance by the
      user.*/
    vdecChannelDesc_ = aclvdecCreateChannelDesc();

    // channelId: 0-15
    ret = aclvdecSetChannelDescChannelId(vdecChannelDesc_, 10);
    ret = aclvdecSetChannelDescThreadId(vdecChannelDesc_, threadId_);
    /* Sets the callback function*/
    ret = aclvdecSetChannelDescCallback(vdecChannelDesc_, callback);

    //The H265_MAIN_LEVEL video encoding protocol is used in the example
    ret = aclvdecSetChannelDescEnType(vdecChannelDesc_, static_cast<acldvppStreamFormat>(enType_));
    //PIXEL_FORMAT_YVU_SEMIPLANAR_420
    ret = aclvdecSetChannelDescOutPicFormat(vdecChannelDesc_, static_cast<acldvppPixelFormat>(format_));

    /* 5.Create video stream processing channel */
    ret = aclvdecCreateChannel(vdecChannelDesc_);

    /* Video decoding processing */
    int rest_len = 0;
    int max_len = 10;
    void *inBufferDev = nullptr;
    uint32_t inBufferSize = 0;
    size_t outPutDataSize = (inputWidth * inputHeight * 3) / 2;

    // vector<string> file_name_v;
    string video_path = "../data/out_video_";
    while (rest_len < max_len) {
        // read file to device memory
        string path = video_path + std::to_string(rest_len) + ".h264";
        INFO_LOG("start to process: %s", path.c_str());
        ReadFileToDeviceMem(path.c_str(), inBufferDev, inBufferSize);
        // Create input video stream description information, set the properties of the stream information
        streamInputDesc_ = acldvppCreateStreamDesc();
        //inBufferDev_ means the memory for input video data by Device, and inBufferSize_ means the memory size
        ret = acldvppSetStreamDescData(streamInputDesc_, inBufferDev);
        ret = acldvppSetStreamDescSize(streamInputDesc_, inBufferSize);


        //Device memory picOutBufferDev_ is used to store output data decoded by VDEC

        ret = acldvppMalloc(&picOutBufferDev_, outPutDataSize);

        //Create output image description information, set the image description information properties
        //picOutputDesc_ is acldvppPicDesc
        picOutputDesc_ = acldvppCreatePicDesc();

        ret = acldvppSetPicDescData(picOutputDesc_, picOutBufferDev_);
        ret = acldvppSetPicDescSize(picOutputDesc_, outPutDataSize);
        ret = acldvppSetPicDescFormat(picOutputDesc_, static_cast<acldvppPixelFormat>(format_));

        //
        aclvdecSendFrame(vdecChannelDesc_, streamInputDesc_, picOutputDesc_, nullptr, nullptr);


        rest_len++;
    }



    // ret = acldvppFree(reinterpret_cast<void *>(picOutBufferDev_));
    ret = acldvppFree(picOutBufferDev_);

    // Release acldvppPicDesc type data, representing output picture description data after decoding
    ret = acldvppDestroyPicDesc(picOutputDesc_);


    acldvppFree(inBufferDev);
    ret = acldvppDestroyStreamDesc(streamInputDesc_);

    ret = aclvdecDestroyChannel(vdecChannelDesc_);
    ret = aclvdecDestroyChannelDesc(vdecChannelDesc_);
    vdecChannelDesc_ = nullptr;



    // destory thread
    runFlag = false;
    void *res = nullptr;
    pthread_join(threadId_, &res);

    ret = aclrtDestroyContext(context_);
    context_ = nullptr;
    ret = aclrtResetDevice(deviceId_);
    ret = aclFinalize();
}

int main(int argc, char* argv[])
{

    DIR *dir;
    if ((dir = opendir("./output")) == NULL)
        system("mkdir ./output");

    // 视频解码
    vdecFunc();


    return SUCCESS;
}