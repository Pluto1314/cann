#pragma once
#include <iostream>
#include <unistd.h>
#include <dirent.h>
#include <fstream>
#include <cstring>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <map>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#include <cstdint>

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO]  " fmt "\n", ##args)
#define WARN_LOG(fmt, args...) fprintf(stdout, "[WARN]  " fmt "\n", ##args)
#define ERROR_LOG(fmt, args...) fprintf(stdout, "[ERROR] " fmt "\n", ##args)

typedef enum Result {
    SUCCESS = 0,
    FAILED = 1
} Result;

typedef struct PicDesc {
    std::string picName;
    int width;
    int height;
}PicDesc;


void vdecFunc();


extern aclrtRunMode runMode;

extern int32_t deviceId_;
extern aclrtContext context_;
extern aclrtStream stream_;
extern pthread_t threadId_;
extern char *outFolder;
extern PicDesc picDesc_;

extern int32_t format_; // 1：YUV420 semi-planner（nv12）; 2：YVU420 semi-planner（nv21）

/* 0：H265 main level
 * 1：H264 baseline level
 * 2：H264 main level
 * 3：H264 high level
 */
extern int32_t enType_;

extern aclvdecChannelDesc *vdecChannelDesc_;
extern acldvppStreamDesc *streamInputDesc_;
extern acldvppPicDesc *picOutputDesc_;
extern void *picOutBufferDev_;
extern void *inBufferDev_;
extern uint32_t inBufferSize_;
extern bool runFlag;

extern std::string filePath;
extern const int inputWidth;
extern const int inputHeight;