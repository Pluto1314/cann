/**
* @file Main.cpp
*
* Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <iostream>
#include "acl/acl.h"
//using namespace std;

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO] " fmt "\n", ##args)

int main(int argc, char* argv[])
{
    INFO_LOG("ACL Hello World.");
    const char* path = "acl.json";
    aclError ret = aclInit(path);
    ret = aclFinalize();

    return 0;
}