#include <cstdint>
#include <iostream>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include "acl/acl.h"

#include "opencv2/opencv.hpp"
#include "acl/ops/acl_dvpp.h"
#include "main.h"


using namespace std;

int32_t deviceId;
aclrtContext context;
aclrtStream stream;
aclrtRunMode runMode;
pthread_t threadId;

aclvencChannelDesc *vencChannelDesc = nullptr;
acldvppPicDesc *vpcInputDesc = nullptr;
aclvencFrameConfig *vencFrameConfig = nullptr;
acldvppStreamDesc *outputStreamDesc = nullptr;
void *codeInputBufferDev = nullptr;
acldvppPixelFormat format;
int32_t enType = 2;
uint32_t inputBufferSize;
FILE *outFileFp;
bool runFlag= true;


void *ThreadFunc(aclrtContext sharedContext)
{
    if (sharedContext == nullptr) {
        ERROR_LOG("sharedContext can not be nullptr");
        return ((void*)(-1));
    }
    INFO_LOG("use shared context for this thread");
    aclError ret = aclrtSetCurrentContext(sharedContext);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSetCurrentContext failed, errorCode = %d", static_cast<int32_t>(ret));
        return ((void*)(-1));
    }
    INFO_LOG("process callback thread start ");
    while (runFlag) {
        // Notice: timeout 1000ms
        (void)aclrtProcessReport(1000);
        //pthread_testcancel();
    }
    return (void*)0;
}

bool WriteToFile(FILE *outFileFp_, const void *dataDev, uint32_t dataSize)
{
    bool ret = true;
    size_t writeRet = fwrite(dataDev, 1, dataSize, outFileFp_);
    if (writeRet != dataSize) {
        ret = false;
    }
    fflush(outFileFp_);

    return ret;
}

void callback(acldvppPicDesc *input, acldvppStreamDesc *outputStreamDesc, void *userdata)
{
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    void *outputDev = acldvppGetStreamDescData(outputStreamDesc);
    uint32_t streamDescSize = acldvppGetStreamDescSize(outputStreamDesc);
    bool ret;

    //
    FILE *outPathFp;
    static int count = 0;

    if (runMode == ACL_HOST) {
        // test 10 pic
        if (count >= 10)
            return;
        INFO_LOG("IN ACL_HOST!");
        void * hostPtr = nullptr;
        aclrtMallocHost(&hostPtr, streamDescSize);
        aclrtMemcpy(hostPtr, streamDescSize, outputDev, streamDescSize, ACL_MEMCPY_DEVICE_TO_HOST);

        // 更改一下输出，注意：这里我运行在Host侧
        std::string outPath = "./output/venc_video_" + std::to_string(count) + ".h265";
        outPathFp = fopen(outPath.c_str(), "wb+");
        if (outPathFp == nullptr) {
            ERROR_LOG("failed to open: %s", outPath.c_str());
            return;
        }
        ret = WriteToFile(outPathFp, hostPtr, streamDescSize);
        fclose(outPathFp);

        //ret = WriteToFile(outFileFp, hostPtr, streamDescSize);
        (void)aclrtFreeHost(hostPtr);
    }
    else{
        INFO_LOG("NOT IN ACL_HOST!");
        ret = WriteToFile(outFileFp, outputDev, streamDescSize);
    }

    if (!ret) {
        ERROR_LOG("write file failed.");
    }
    INFO_LOG("success to callback, stream size:%u", streamDescSize);
    count++;
}

Result InitResource() {
    const char *aclConfigPath = "../src/acl.json";
    aclError ret = aclInit(aclConfigPath);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("Acl init failed");
        return FAILED;
    }
    INFO_LOG("Acl init success");

    ret = aclrtSetDevice(deviceId);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("Acl open device %d failed", deviceId);
        return FAILED;
    }
    INFO_LOG("Open device %d success", deviceId);

    ret = aclrtCreateContext(&context, deviceId);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acl create context failed");
        return FAILED;
    }
    INFO_LOG("create context success");

    //Gets whether the current application is running on host or Device
    ret = aclrtGetRunMode(&runMode);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acl get run mode failed");
        return FAILED;
    }
    return SUCCESS;
}

Result Init(int imgWidth, int imgHeight){

    InitResource();

    pthread_create(&threadId, nullptr, ThreadFunc, context);
    int width = imgWidth;
    int height = imgHeight;
    uint32_t alignWidth = ALIGN_UP128(width);
    uint32_t alignHeight = ALIGN_UP16(height);
    if (alignWidth == 0 || alignHeight == 0) {
        ERROR_LOG("InitCodeInputDesc AlignmentHelper failed. image w %d, h %d, align w%u, h%u",
        width, height, alignWidth, alignHeight);
        return FAILED;
    }
    //Allocate a large enough memory
    inputBufferSize = YUV420SP_SIZE(alignWidth, alignHeight);
    aclError ret = acldvppMalloc(&codeInputBufferDev, inputBufferSize);

    format = static_cast<acldvppPixelFormat>(PIXEL_FORMAT_YVU_SEMIPLANAR_420);
    vencFrameConfig = aclvencCreateFrameConfig();
    aclvencSetFrameConfigForceIFrame(vencFrameConfig, 0);
    if (vencFrameConfig == nullptr) {
        ERROR_LOG("Dvpp init failed for create config failed");
        return FAILED;
    }

    vencChannelDesc = aclvencCreateChannelDesc();
    if (vencChannelDesc == nullptr) {
        ERROR_LOG("aclvencCreateChannelDesc failed");
        return FAILED;
    }
    ret = aclvencSetChannelDescThreadId(vencChannelDesc, threadId);
    ret = aclvencSetChannelDescCallback(vencChannelDesc, callback);
    ret = aclvencSetChannelDescEnType(vencChannelDesc, static_cast<acldvppStreamFormat>(enType));
    ret = aclvencSetChannelDescPicFormat(vencChannelDesc, format);
    ret = aclvencSetChannelDescKeyFrameInterval(vencChannelDesc, 1);
    ret = aclvencSetChannelDescPicWidth(vencChannelDesc, width);
    ret = aclvencSetChannelDescPicHeight(vencChannelDesc, height);
    ret = aclvencCreateChannel(vencChannelDesc);

    vpcInputDesc = acldvppCreatePicDesc();
    if (vpcInputDesc == nullptr) {
        ERROR_LOG("acldvppCreatePicDesc vpcInputDesc failed");
        return FAILED;
    }
    ret = acldvppSetPicDescFormat(vpcInputDesc, format);
    ret = acldvppSetPicDescWidth(vpcInputDesc, width);
    ret = acldvppSetPicDescHeight(vpcInputDesc, height);
    ret = acldvppSetPicDescWidthStride(vpcInputDesc, alignWidth);
    ret = acldvppSetPicDescHeightStride(vpcInputDesc, alignHeight);
    INFO_LOG("dvpp init resource ok");
    return SUCCESS;
}

Result Process(cv::Mat& srcImage) {
    aclError ret;
    if(runMode == ACL_HOST) {
        ret = aclrtMemcpy(codeInputBufferDev, inputBufferSize, srcImage.data, inputBufferSize, ACL_MEMCPY_HOST_TO_DEVICE);
    }
    else {
        ret = aclrtMemcpy(codeInputBufferDev, inputBufferSize, srcImage.data, inputBufferSize, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }

    ret = acldvppSetPicDescData(vpcInputDesc, codeInputBufferDev);
    ret = acldvppSetPicDescSize(vpcInputDesc, inputBufferSize);

    ret = aclvencSendFrame(vencChannelDesc, vpcInputDesc,
    static_cast<void *>(outputStreamDesc), vencFrameConfig, nullptr);
    return SUCCESS;
}

void DestroyAclResource(){
    aclError ret;
    if (context != nullptr) {
        ret = aclrtDestroyContext(context);
        if (ret != ACL_SUCCESS) {
            ERROR_LOG("destroy context failed");
        }
        context = nullptr;
    }
    INFO_LOG("end to destroy context");

    ret = aclrtResetDevice(deviceId);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("reset device failed");
    }
    INFO_LOG("end to reset device is %d", deviceId);

    ret = aclFinalize();
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("finalize acl failed");
    }
    INFO_LOG("end to finalize acl");
}

void DestroyResource(){

    aclvencSetFrameConfigEos(vencFrameConfig, 1);
    aclvencSetFrameConfigForceIFrame(vencFrameConfig, 0);
    aclvencSendFrame(vencChannelDesc, nullptr, nullptr, vencFrameConfig, nullptr);

    if (vencFrameConfig != nullptr) {
        (void)aclvencDestroyFrameConfig(vencFrameConfig);
        vencFrameConfig = nullptr;
    }

    if (vpcInputDesc != nullptr) {
        (void)acldvppDestroyPicDesc(vpcInputDesc);
        vpcInputDesc = nullptr;
    }

    if (codeInputBufferDev != nullptr) {
        (void)acldvppFree(codeInputBufferDev);
        codeInputBufferDev = nullptr;
    }

    if (outputStreamDesc != nullptr) {
        (void)acldvppDestroyStreamDesc(outputStreamDesc);
        outputStreamDesc = nullptr;
    }

    aclError aclRet;
    if (vencChannelDesc != nullptr) {
        aclRet = aclvencDestroyChannel(vencChannelDesc);
        if (aclRet != ACL_SUCCESS) {
            ERROR_LOG("aclvencDestroyChannel failed, aclRet = %d", aclRet);
        }
        (void)aclvencDestroyChannelDesc(vencChannelDesc);
        vencChannelDesc = nullptr;
    }

    runFlag = false;
    void *res = nullptr;
    pthread_cancel(threadId);
    pthread_join(threadId, &res);
    fclose(outFileFp);
    DestroyAclResource();
    INFO_LOG("end to destroy Resource");
}

int main(int argc, char *argv[]) {
    //Use Opencv to open the video file
    string videoFile = "../data/detection.mp4";
    printf("open %s\n", videoFile.c_str());
    cv::VideoCapture capture(videoFile);
    if (!capture.isOpened()) {
        cout << "Movie open Error" << endl;
        return FAILED;
    }
    cout << "Movie open success" << endl;

    int imgHeight = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
    int imgWidth = static_cast<int>(capture.get(cv::CAP_PROP_FRAME_WIDTH));

    INFO_LOG("imgWidth : %d", imgWidth);
    INFO_LOG("imgHeight : %d", imgHeight);

    //open the output file
    string outputPath = "./output/out_video.h264";
    outFileFp = fopen(outputPath.c_str(), "ab");
    if(outFileFp == nullptr)    {
        ERROR_LOG("Failed to open  file %s.", outputPath.c_str());
        return FAILED;
    }

    Init(imgWidth, imgHeight);
    cv::Mat frame;
    while(1) {
        //Read a frame of an image
        if (!capture.read(frame)) {
            INFO_LOG("Video capture return false");
            break;
        }
        int cols = frame.cols;
        int rows = frame.rows;
        int Yindex = 0;
        int UVindex = rows * cols;
        cv::Mat NV21(rows+rows/2, cols, CV_8UC1);
        int UVRow{ 0 };
        for (int i=0;i<rows;i++)
        {
            for (int j=0;j<cols;j++)
            {
                uchar* YPointer = NV21.ptr<uchar>(i);
                int B = frame.at<cv::Vec3b>(i, j)[0];
                int G = frame.at<cv::Vec3b>(i, j)[1];
                int R = frame.at<cv::Vec3b>(i, j)[2];
                int Y = (77 * R + 150 * G + 29 * B) >> 8;
                YPointer[j] = Y;
                uchar* UVPointer = NV21.ptr<uchar>(rows+i/2);
                if (i%2==0&&(j)%2==0)
                {
                    int U = ((-44 * R - 87 * G + 131 * B) >> 8) + 128;
                    int V = ((131 * R - 110 * G - 21 * B) >> 8) + 128;
                    UVPointer[j] = V;
                    UVPointer[j+1] = U;
                }
            }
        }
        Process(NV21);
    }
    DestroyResource();
    INFO_LOG("Execute video object detection success");
    return SUCCESS;
}