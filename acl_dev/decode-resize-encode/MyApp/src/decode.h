#pragma once


void* GetDeviceBufferOfPicture(const PicDesc &picDesc, uint32_t &devPicBufferSize);
Result SaveDvppOutputDataDecode(const char *fileName, const void *devPtr, uint32_t dataSize);
void SetInput(void *inDevBuffer, int inDevBufferSize, int inputWidth, int inputHeight);