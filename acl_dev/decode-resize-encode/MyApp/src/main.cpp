/**
* @file Main.cpp
*
* Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include "main.h"
#include "decode.h"
#include "resize.h"
#include "encode.h"
#include <string>

// 资源管理变量
int32_t deviceId_ = 0;
aclrtContext context_ = nullptr;
aclrtStream stream_ = nullptr;
acldvppChannelDesc *dvppChannelDesc_;
aclrtRunMode runMode;

// 输出
void *decodeOutDevBuffer_;
acldvppPicDesc *decodeOutputDesc_;
uint32_t decodeDataSize_;
void* encode_out_buffer_dev_;

// 输入
void *inDevBuffer_;
uint32_t inDevBufferSize_;
acldvppPicDesc *encodeInputDesc_;

uint32_t inputWidth_;
uint32_t inputHeight_;
uint32_t inBufferSize;

uint32_t in_devbuffer_size_encode_;
uint32_t encode_outbuffer_size_;

acldvppStreamDesc *streamInputDesc_;
acldvppPicDesc *picOutputDesc_;

// config
acldvppJpegeConfig *jpegeConfig_;

PicDesc inPicDesc;
PicDesc outPicDesc;

using namespace std;

void DecodeFunc(string image_path) {
    uint32_t image_width = 0;
    uint32_t image_height = 0;
    PicDesc testPic = {image_path, image_width, image_height};

    INFO_LOG("start to process picture:%s", testPic.picName.c_str());
    // dvpp process
    /*3.Read the picture into memory. InDevBuffer_ indicates the memory for storing the input picture,
	inDevBufferSize indicates the memory size, please apply for the input memory in advance*/
    uint32_t devPicBufferSize;
    void *picDevBuffer = GetDeviceBufferOfPicture(testPic, devPicBufferSize);
    if (picDevBuffer == nullptr) {
        ERROR_LOG("get pic device buffer failed,index is 0");
        //return FAILED;
    }
    //4.Create image data processing channel
    dvppChannelDesc_ = acldvppCreateChannelDesc();
    acldvppCreateChannel(dvppChannelDesc_);
    INFO_LOG("dvpp init resource success");


    SetInput(picDevBuffer, devPicBufferSize, testPic.width, testPic.height);

    // InitDecodeOutputDesc
    uint32_t decodeOutWidthStride = (inputWidth_ + 127) / 128 * 128; // 128-byte alignment
    uint32_t decodeOutHeightStride = (inputHeight_ + 15) / 16 * 16; // 16-byte alignment

    // use acldvppJpegPredictDecSize to get output size.
    // uint32_t decodeOutBufferSize = decodeOutWidthStride * decodeOutHeightStride * 3 / 2; // yuv format size
    // uint32_t decodeOutBufferSize = testPic.jpegDecodeSize;
    aclError ret = acldvppMalloc(&decodeOutDevBuffer_, testPic.jpegDecodeSize);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acldvppMalloc jpegOutBufferDev failed, ret = %d", ret);
        //return FAILED;
    }

    decodeOutputDesc_ = acldvppCreatePicDesc();
    if (decodeOutputDesc_ == nullptr) {
        ERROR_LOG("acldvppCreatePicDesc decodeOutputDesc failed");
        //return FAILED;
    }

    acldvppSetPicDescData(decodeOutputDesc_, decodeOutDevBuffer_);
    // here the format shoud be same with the value you set when you get decodeOutBufferSize from
    acldvppSetPicDescFormat(decodeOutputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(decodeOutputDesc_, inputWidth_);
    acldvppSetPicDescHeight(decodeOutputDesc_, inputHeight_);
    acldvppSetPicDescWidthStride(decodeOutputDesc_, decodeOutWidthStride);
    acldvppSetPicDescHeightStride(decodeOutputDesc_, decodeOutHeightStride);
    acldvppSetPicDescSize(decodeOutputDesc_, testPic.jpegDecodeSize);

    ret = acldvppJpegDecodeAsync(dvppChannelDesc_, inDevBuffer_, inDevBufferSize_,
    decodeOutputDesc_, stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acldvppJpegDecodeAsync failed, ret = %d", ret);
        //return FAILED;
    }

    ret = aclrtSynchronizeStream(stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSynchronizeStream failed");
        //return FAILED;
    }

    decodeDataSize_ = acldvppGetPicDescSize(decodeOutputDesc_);

    (void)acldvppFree(picDevBuffer);
    picDevBuffer = nullptr;

    int dir_tail_index = image_path.find("/data");
    std::string outfile_dir = image_path.substr(0, dir_tail_index) + "/" + "out/output/";
    std::string outfile_path = outfile_dir + image_path.substr(dir_tail_index+5+1, image_path.rfind(".jpg")-dir_tail_index-5-1)
    + "_jpegd_" + std::to_string(testPic.width) + "_" + std::to_string(testPic.height) + ".yuv";
    INFO_LOG("Decode outfile_path=%s", outfile_path.c_str());


    ret = SaveDvppOutputDataDecode(outfile_path.c_str(), decodeOutDevBuffer_, decodeDataSize_);
    if (ret != SUCCESS) {
        ERROR_LOG("save dvpp output data failed");
        // allow not return
    }

    inDevBuffer_ = nullptr;
    if (decodeOutputDesc_ != nullptr) {
        acldvppDestroyPicDesc(decodeOutputDesc_);
        decodeOutputDesc_ = nullptr;
    }

    // output fileName
//    string decodePicFileName = image_path.substr(dir_tail_index+5+1, image_path.rfind(".jpg")-dir_tail_index-5-1)
//    + "_jpegd_" + std::to_string(testPic.width) + "_" + std::to_string(testPic.height) + ".yuv";
//    return decodePicFileName;
}

void ResizeFunc(string image_path, string output_path, int inw, int inh, int outw, int outh) {
    /* 3.Initialization parameters: width and height of the original image, crop width and height. Initialize folder: Output folder*/
    Initparam(image_path, output_path, inw, inh, outw, outh);
    const int modelInputWidth = outPicDesc.width; // cur model shape is 224 * 224
    const int modelInputHeight = outPicDesc.height;

    /* 4. Channel description information when creating image data processing channels, dvppChannelDesc_ is acldvppChannelDesc type*/
    dvppChannelDesc_ = acldvppCreateChannelDesc();

    /* 5. Create the image data processing channel.*/
    acldvppCreateChannel(dvppChannelDesc_);

    // GetPicDevBuffer4JpegD
    uint32_t inputBuffSize = 0;
    char* inputBuff = ReadBinFileResize(inPicDesc.picName, inputBuffSize);
    void *inBufferDev = nullptr;
    inBufferSize = inputBuffSize;
    acldvppMalloc(&inBufferDev, inBufferSize);
    if (runMode == ACL_HOST) {
        aclrtMemcpy(inBufferDev, inBufferSize, inputBuff, inputBuffSize, ACL_MEMCPY_HOST_TO_DEVICE);
    }
    else {
        aclrtMemcpy(inBufferDev, inBufferSize, inputBuff, inputBuffSize, ACL_MEMCPY_DEVICE_TO_DEVICE);
    }
    delete[] inputBuff;

    acldvppResizeConfig *resizeConfig_ = acldvppCreateResizeConfig();

    /* processdecode*/
    inputWidth_ = inPicDesc.width;
    inputHeight_ = inPicDesc.height;
    uint32_t sizeAlignment = 3;
    uint32_t sizeNum = 2;
    // if the input yuv is from JPEGD, it should be aligned to 128*16
    // if the input yuv is from VDEC, it shoud be aligned to 16*2
    uint32_t inputWidthStride = AlignmentHelper(inputWidth_, 128);
    uint32_t inputHeightStride = AlignmentHelper(inputHeight_, 16);
    uint32_t inputBufferSize = inputWidthStride * inputHeightStride * sizeAlignment / sizeNum;
    acldvppPicDesc *vpcInputDesc_ = acldvppCreatePicDesc();
    acldvppPicDesc *vpcOutputDesc_ = acldvppCreatePicDesc();
    void *vpcOutBufferDev_ = nullptr;
    acldvppSetPicDescData(vpcInputDesc_, reinterpret_cast<char *>(inBufferDev));
    // the format is the input yuv's format.
    acldvppSetPicDescFormat(vpcInputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(vpcInputDesc_, inputWidth_);
    acldvppSetPicDescHeight(vpcInputDesc_, inputHeight_);
    acldvppSetPicDescWidthStride(vpcInputDesc_, inputWidthStride);
    acldvppSetPicDescHeightStride(vpcInputDesc_, inputHeightStride);
    acldvppSetPicDescSize(vpcInputDesc_, inputBufferSize);

    // here is the VPC constraints, should be aligned to 16*2
    int resizeOutWidthStride = AlignmentHelper(modelInputWidth, 16);
    int resizeOutHeightStride = AlignmentHelper(modelInputHeight, 2);
    uint32_t vpcOutBufferSize_ = resizeOutWidthStride * resizeOutHeightStride * sizeAlignment / sizeNum;
    acldvppMalloc(&vpcOutBufferDev_, vpcOutBufferSize_);
    acldvppSetPicDescData(vpcOutputDesc_, vpcOutBufferDev_);
    // the format should be under the VPC constraints(only support the following 2):
    // PIXEL_FORMAT_YUV_SEMIPLANAR_420 = 1
    // PIXEL_FORMAT_YVU_SEMIPLANAR_420 = 2
    acldvppSetPicDescFormat(vpcOutputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(vpcOutputDesc_, modelInputWidth);
    acldvppSetPicDescHeight(vpcOutputDesc_, modelInputHeight);
    acldvppSetPicDescWidthStride(vpcOutputDesc_, resizeOutWidthStride);
    acldvppSetPicDescHeightStride(vpcOutputDesc_, resizeOutHeightStride);
    acldvppSetPicDescSize(vpcOutputDesc_, vpcOutBufferSize_);

    aclError ret = acldvppVpcResizeAsync(dvppChannelDesc_, vpcInputDesc_,
    vpcOutputDesc_, resizeConfig_, stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("acldvppVpcResizeAsync failed, ret = %d", ret);
        //return FAILED;
    }

    aclrtSynchronizeStream(stream_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSynchronizeStream failed");
        //return FAILED;
    }

    /* DestroyResizeResource*/
    (void)acldvppDestroyResizeConfig(resizeConfig_);
    resizeConfig_ = nullptr;
    (void)acldvppDestroyPicDesc(vpcInputDesc_);
    vpcInputDesc_ = nullptr;
    (void)acldvppDestroyPicDesc(vpcOutputDesc_);
    vpcOutputDesc_ = nullptr;

    (void)acldvppFree(inBufferDev);
    inBufferDev = nullptr;

    SaveDvppOutputDataResize(outPicDesc.picName.c_str(), vpcOutBufferDev_, vpcOutBufferSize_);
    if (vpcOutBufferDev_ != nullptr) {
        (void)acldvppFree(vpcOutBufferDev_);
        vpcOutBufferDev_ = nullptr;
    }
    (void)acldvppDestroyChannel(dvppChannelDesc_);
    (void)acldvppDestroyChannelDesc(dvppChannelDesc_);
    dvppChannelDesc_ = nullptr;

//    return outPicDesc.picName;
}

void EncodeFunc(string image_path, uint32_t image_width, uint32_t image_height) {

    uint32_t encodeLevel = 100; // default optimal level (0-100)
    PicDesc testPic = {image_path, image_width, image_height};
    INFO_LOG("Start to process picture: %s", testPic.picName.c_str());
    INFO_LOG("Call JpegE");

//    DIR *dir;
//    if ((dir = opendir("./output")) == NULL){
//        system("mkdir ./output");
//    }

    //create dvpp channel
    dvppChannelDesc_ = acldvppCreateChannelDesc();
    INFO_LOG("Call acldvppCreateChannelDesc success");
    acldvppCreateChannel(dvppChannelDesc_);
    INFO_LOG("Call acldvppCreateChannel success");
    INFO_LOG("DVPP init resource success");

    uint32_t jpegInBufferSize;
    jpegInBufferSize = compute_encode_inputsize(testPic.width, testPic.height);
    //input mem malloc
    char* picDevBuffer = get_picdevbuffer4_jpege(testPic, jpegInBufferSize);
    if (nullptr == picDevBuffer) {
        ERROR_LOG("get picDevBuffer failed, index is %d", 0);
        //return FAILED;
    }
    set_input4_jpege(*picDevBuffer, jpegInBufferSize, testPic.width, testPic.height);
    //create pic desc & set pic attrs
    uint32_t widthAlignment = 16;
    uint32_t heightAlignment = 2;
    uint32_t encodeInWidthStride = alignment_helper(inputWidth_, widthAlignment);
    uint32_t encodeInHeightStride = alignment_helper(inputHeight_, heightAlignment);
    if (encodeInWidthStride == 0 || encodeInHeightStride == 0) {
        ERROR_LOG("InitEncodeInputDesc AlignmentHelper failed");
        //return FAILED;
    }
    encodeInputDesc_ = acldvppCreatePicDesc();
    INFO_LOG("Call acldvppCreatePicDesc success");
    if (encodeInputDesc_ == nullptr) {
        ERROR_LOG("acldvppCreatePicDesc encodeInputDesc_ failed");
        //return FAILED;
    }

    acldvppSetPicDescData(encodeInputDesc_, reinterpret_cast<void *>(inDevBuffer_));
    acldvppSetPicDescFormat(encodeInputDesc_, PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    acldvppSetPicDescWidth(encodeInputDesc_, inputWidth_);
    acldvppSetPicDescHeight(encodeInputDesc_, inputHeight_);
    acldvppSetPicDescWidthStride(encodeInputDesc_, encodeInWidthStride);
    acldvppSetPicDescHeightStride(encodeInputDesc_, encodeInHeightStride);
    acldvppSetPicDescSize(encodeInputDesc_, in_devbuffer_size_encode_);

    jpegeConfig_ = acldvppCreateJpegeConfig();
    INFO_LOG("Call acldvppCreateJpegeConfig success");
    acldvppSetJpegeConfigLevel(jpegeConfig_, encodeLevel);

    //output mem malloc
    acldvppJpegPredictEncSize(encodeInputDesc_, jpegeConfig_, &encode_outbuffer_size_);
    aclError aclRet = acldvppMalloc(&encode_out_buffer_dev_, encode_outbuffer_size_);

    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("malloc encodeOutBufferDev_ failed, aclRet is %d", aclRet);
        //return FAILED;
    }

    //call Asynchronous api
    aclRet = acldvppJpegEncodeAsync(dvppChannelDesc_, encodeInputDesc_, encode_out_buffer_dev_,
    &encode_outbuffer_size_, jpegeConfig_, stream_);
    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("acldvppJpegEncodeAsync failed, aclRet = %d", aclRet);
        //return FAILED;
    }
    INFO_LOG("Call acldvppJpegEncodeAsync success");
    aclRet = aclrtSynchronizeStream(stream_);
    if (aclRet != ACL_SUCCESS) {
        ERROR_LOG("encode aclrtSynchronizeStream failed, aclRet = %d", aclRet);
        //return FAILED;
    }

    //malloc host mem & save pic

//    int dir_tail_index = image_path.find("/data");
//    std::string outfile_dir = image_path.substr(0, dir_tail_index) + "/" + "out/output/";
//    std::string outfile_path = outfile_dir + image_path.substr(dir_tail_index+5+1, image_path.rfind(".yuv")-dir_tail_index-5-1)
//    + "_jpege_" + std::to_string(inputWidth_) + "_" + std::to_string(inputHeight_) + ".jpg";
    std::string outfile_path = "../out/output/resize_224_224.jpg";
    INFO_LOG("Encode outfile_path=%s", outfile_path.c_str());

    // std::string encodeOutFileName = image_path.replace(image_path.rfind(".yuv"), 4, "_jpege_output");
    // encodeOutFileName = encodeOutFileName + ".jpg";
    Result ret = save_dvpp_outputdata(outfile_path.c_str(), encode_out_buffer_dev_, encode_outbuffer_size_);
    if (ret != SUCCESS) {
        ERROR_LOG("save encode output data failed.");
        //return FAILED;
    }

    if (jpegeConfig_ != nullptr) {
        (void)acldvppDestroyJpegeConfig(jpegeConfig_);
        jpegeConfig_ = nullptr;
    }
    INFO_LOG("Call acldvppDestroyJpegeConfig success");
    if (encodeInputDesc_ != nullptr) {
        (void)acldvppDestroyPicDesc(encodeInputDesc_);
        encodeInputDesc_ = nullptr;
    }
    INFO_LOG("Call acldvppDestroyPicDesc success");
    if (inDevBuffer_ != nullptr) {
        (void)acldvppFree(inDevBuffer_);
        inDevBuffer_ = nullptr;
    }
    INFO_LOG("Call acldvppFree success");

//    string out_name = image_path.substr(dir_tail_index+5+1, image_path.rfind(".yuv")-dir_tail_index-5-1)
//    + "_jpege_" + std::to_string(inputWidth_) + "_" + std::to_string(inputHeight_) + ".jpg";
//    return out_name;
}

void DestroyResource()
{
    aclError ret;

    // DestroyResource

    if (stream_ != nullptr) {
        ret = aclrtDestroyStream(stream_);
        if (ret != ACL_SUCCESS) {
            ERROR_LOG("destroy stream failed");
        }
        stream_ = nullptr;
    }
    INFO_LOG("end to destroy stream");

    if (context_ != nullptr) {
        ret = aclrtDestroyContext(context_);
        if (ret != ACL_SUCCESS) {
            ERROR_LOG("destroy context failed");
        }
        context_ = nullptr;
    }
    INFO_LOG("end to destroy context");

    ret = aclrtResetDevice(deviceId_);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("reset device failed");
    }
    INFO_LOG("end to reset device is %d", deviceId_);

    ret = aclFinalize();
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("finalize acl failed");
    }
    INFO_LOG("end to finalize acl");
}

int main(int argc, char* argv[])
{
    // 1. acl init
    const char *aclConfigPath = "../src/acl.json";
    aclInit(aclConfigPath);
    INFO_LOG("acl init success");

    // 2. create Device,Context,Stream
    aclrtSetDevice(deviceId_);
    aclrtCreateContext(&context_, deviceId_);
    aclrtCreateStream(&stream_);
    aclrtGetRunMode(&runMode);
    INFO_LOG("create stream success");

    // 3. decode
    string image_path = "../data/bb_V0003_I0001800.jpg";
    //string decodeOutputFileName = DecodeFunc(image_path);
    DecodeFunc(image_path);

    // 4. resize
    //string resizeOutputFileName = ResizeFunc(decodeOutputFileName, "resize.jpg", 1920, 1080, 224, 224);
    ResizeFunc("../out/output/bb_V0003_I0001800_jpegd_1920_1080.yuv", "../out/output/resize.yuv", 1920, 1080, 224, 224);

    // 5. encode
    //string encodeOutputFileName = EncodeFunc(resizeOutputFileName, 224, 224);
    EncodeFunc("../out/output/resize.yuv", 224, 224);

    // 6. destroyResource
    DestroyResource();

    INFO_LOG("execute sample success");
    return SUCCESS;
}