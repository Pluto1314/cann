# Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.

# CMake lowest version requirement
cmake_minimum_required(VERSION 3.5.1)

# project information
project(MyApp)

# Compile options
add_compile_options(-std=c++11)

add_definitions(-DENABLE_DVPP_INTERFACE)

# Specify target generation path
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY  "../../../out")
set(CMAKE_CXX_FLAGS_DEBUG "-fPIC -O0 -g -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "-fPIC -O2 -Wall")

# Header path
set(INC_PATH $ENV{DDK_PATH})
if (NOT DEFINED ENV{DDK_PATH})
    if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Windows")
        set(INC_PATH "C:/Program Files/HuaWei/Ascend")
    else ()
        set(INC_PATH "/usr/local/Ascend")
    endif ()
    message(STATUS "set default INC_PATH: ${INC_PATH}")
else ()
    message(STATUS "env INC_PATH: ${INC_PATH}")
endif ()
if (EXISTS ${INC_PATH}/runtime)
    set(RUNTIME_NAME "runtime")
else ()
    set(RUNTIME_NAME "acllib")
endif()
message(STATUS "set runtime package name: ${RUNTIME_NAME}")

include_directories(
    ${INC_PATH}/${RUNTIME_NAME}/include/
)
# add host lib path
link_directories(
	$ENV{INSTALL_DIR}/runtime/lib64/stub
)
set(LIB_PATH $ENV{NPU_HOST_LIB})
# Dynamic libraries in the stub directory can only be used for compilation
if (NOT DEFINED ENV{NPU_HOST_LIB})
    if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Windows")
        set(LIB_PATH "C:/Program Files/HuaWei/Ascend/Acllib/lib64")
    else ()
        set(LIB_PATH "/usr/local/Ascend/acllib/lib64/stub/")
    endif ()
    message(STATUS "set default LIB_PATH: ${LIB_PATH}")
else ()
    message(STATUS "env LIB_PATH: ${LIB_PATH}")
endif ()

link_directories(${LIB_PATH})

# Header path
include_directories(
    ${INC_PATH}/acllib/include/
    ../inc/
)

add_executable(main
        decode.cpp
        resize.cpp
        encode.cpp
        main.cpp)

target_link_libraries(main ascendcl acl_cblas acl_dvpp stdc++)

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Windows")
    target_link_libraries(main
        libascendcl)
else ()
    if (${target} MATCHES "Simulator_Function")
        target_link_libraries(main
            funcsim)
    else ()
        target_link_libraries(main
            ascendcl stdc++)
    endif ()
endif ()

install(TARGETS main DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
