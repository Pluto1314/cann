#pragma once
#include <unistd.h>
#include <dirent.h>
#include <fstream>
#include <cstring>
#include <vector>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <map>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#include <cstdint>
#include <iostream>
#include <stdlib.h>

using namespace std;

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO]  " fmt "\n", ##args)
#define WARN_LOG(fmt, args...) fprintf(stdout, "[WARN]  " fmt "\n", ##args)
#define ERROR_LOG(fmt, args...) fprintf(stdout, "[ERROR] " fmt "\n", ##args)


enum Result {
    SUCCESS = 0,
    FAILED = 1
};


struct PicDesc {
    string picName;
    uint32_t width;
    uint32_t height;
    uint32_t jpegDecodeSize;
};


void DestroyResource();
void DecodeFunc(string image_path);
void ResizeFunc(string image_path, string output_path, int inw, int inh, int outw, int outh);
void EncodeFunc(string image_path, uint32_t image_width, uint32_t image_height);


// 资源管理变量
extern int32_t deviceId_;
extern aclrtContext context_;
extern aclrtStream stream_;
extern acldvppChannelDesc *dvppChannelDesc_;
extern aclrtRunMode runMode;

// 输出
extern void *decodeOutDevBuffer_;
extern acldvppPicDesc *decodeOutputDesc_;
extern uint32_t decodeDataSize_;
extern void* encode_out_buffer_dev_;

// 输入
extern void *inDevBuffer_;
extern uint32_t inDevBufferSize_;
extern acldvppPicDesc *encodeInputDesc_;

extern uint32_t inputWidth_;
extern uint32_t inputHeight_;
extern uint32_t inBufferSize;

extern uint32_t in_devbuffer_size_encode_;
extern uint32_t encode_outbuffer_size_;

extern acldvppStreamDesc *streamInputDesc_;
extern acldvppPicDesc *picOutputDesc_;

// config
extern acldvppJpegeConfig *jpegeConfig_;

extern PicDesc inPicDesc;
extern PicDesc outPicDesc;